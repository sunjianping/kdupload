'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var node = {
  element: '',
  next: ''
};

var chainTable = function () {
  function chainTable() {
    _classCallCheck(this, chainTable);

    this.header = _extends({}, node);
  }

  _createClass(chainTable, [{
    key: 'insert',
    value: function insert(x) {
      var p = this.header;
      while (p.element !== '' && p.element !== x) {
        if (p.next === '') {
          p.next = _extends({}, node);
        }
        p = p.next;
      }
      p.element = x;
    }
  }, {
    key: 'find',
    value: function find(x) {
      var p = this.header;
      while (p.element !== x && p.next !== '') {
        p = p.next;
      }
      return p;
    }
  }, {
    key: 'findPrevious',
    value: function findPrevious(x) {
      var p = this.header;
      while (p.next.element !== x && p.next !== '') {
        p = p.next;
      }
      return p;
    }
  }, {
    key: 'delete',
    value: function _delete(x) {
      var p = this.findPrevious(x);
      if (p) {
        var cur = p.next;
        p.next = cur.next;
      }
    }
  }]);

  return chainTable;
}();

exports.default = chainTable;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL21vZHVsZS9hZHQvY2hhaW5UYWJsZS5qcyJdLCJuYW1lcyI6WyJub2RlIiwiZWxlbWVudCIsIm5leHQiLCJjaGFpblRhYmxlIiwiaGVhZGVyIiwieCIsInAiLCJmaW5kUHJldmlvdXMiLCJjdXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUNBLElBQU1BLE9BQU87QUFDWEMsV0FBUSxFQURHO0FBRVhDLFFBQUs7QUFGTSxDQUFiOztJQUlxQkMsVTtBQUNuQix3QkFBYTtBQUFBOztBQUNYLFNBQUtDLE1BQUwsZ0JBQWtCSixJQUFsQjtBQUNEOzs7OzJCQUNNSyxDLEVBQUU7QUFDUCxVQUFJQyxJQUFJLEtBQUtGLE1BQWI7QUFDQSxhQUFNRSxFQUFFTCxPQUFGLEtBQVksRUFBWixJQUFnQkssRUFBRUwsT0FBRixLQUFZSSxDQUFsQyxFQUFvQztBQUNqQyxZQUFHQyxFQUFFSixJQUFGLEtBQVMsRUFBWixFQUFlO0FBQ2JJLFlBQUVKLElBQUYsZ0JBQWFGLElBQWI7QUFDRDtBQUNETSxZQUFJQSxFQUFFSixJQUFOO0FBQ0Y7QUFDREksUUFBRUwsT0FBRixHQUFZSSxDQUFaO0FBQ0Q7Ozt5QkFDSUEsQyxFQUFFO0FBQ0wsVUFBSUMsSUFBSSxLQUFLRixNQUFiO0FBQ0EsYUFBTUUsRUFBRUwsT0FBRixLQUFZSSxDQUFaLElBQWVDLEVBQUVKLElBQUYsS0FBUyxFQUE5QixFQUFpQztBQUMvQkksWUFBSUEsRUFBRUosSUFBTjtBQUNEO0FBQ0QsYUFBT0ksQ0FBUDtBQUNEOzs7aUNBQ1lELEMsRUFBRTtBQUNiLFVBQUlDLElBQUksS0FBS0YsTUFBYjtBQUNBLGFBQU1FLEVBQUVKLElBQUYsQ0FBT0QsT0FBUCxLQUFpQkksQ0FBakIsSUFBb0JDLEVBQUVKLElBQUYsS0FBUyxFQUFuQyxFQUFzQztBQUNwQ0ksWUFBSUEsRUFBRUosSUFBTjtBQUNEO0FBQ0QsYUFBT0ksQ0FBUDtBQUNEOzs7NEJBQ01ELEMsRUFBRTtBQUNQLFVBQUlDLElBQUksS0FBS0MsWUFBTCxDQUFrQkYsQ0FBbEIsQ0FBUjtBQUNBLFVBQUdDLENBQUgsRUFBSztBQUNGLFlBQUlFLE1BQU1GLEVBQUVKLElBQVo7QUFDQUksVUFBRUosSUFBRixHQUFTTSxJQUFJTixJQUFiO0FBQ0Y7QUFDRjs7Ozs7O2tCQWxDa0JDLFUiLCJmaWxlIjoiY2hhaW5UYWJsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuY29uc3Qgbm9kZSA9IHtcbiAgZWxlbWVudDonJyxcbiAgbmV4dDonJ1xufVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgY2hhaW5UYWJsZXtcbiAgY29uc3RydWN0b3IoKXtcbiAgICB0aGlzLmhlYWRlciA9IHsuLi5ub2RlfVxuICB9XG4gIGluc2VydCh4KXtcbiAgICBsZXQgcCA9IHRoaXMuaGVhZGVyXG4gICAgd2hpbGUocC5lbGVtZW50IT09JycmJnAuZWxlbWVudCE9PXgpe1xuICAgICAgIGlmKHAubmV4dD09PScnKXtcbiAgICAgICAgIHAubmV4dCA9IHsuLi5ub2RlfVxuICAgICAgIH1cbiAgICAgICBwID0gcC5uZXh0XG4gICAgfVxuICAgIHAuZWxlbWVudCA9IHhcbiAgfVxuICBmaW5kKHgpe1xuICAgIGxldCBwID0gdGhpcy5oZWFkZXJcbiAgICB3aGlsZShwLmVsZW1lbnQhPT14JiZwLm5leHQhPT0nJyl7XG4gICAgICBwID0gcC5uZXh0XG4gICAgfVxuICAgIHJldHVybiBwXG4gIH1cbiAgZmluZFByZXZpb3VzKHgpe1xuICAgIGxldCBwID0gdGhpcy5oZWFkZXJcbiAgICB3aGlsZShwLm5leHQuZWxlbWVudCE9PXgmJnAubmV4dCE9PScnKXtcbiAgICAgIHAgPSBwLm5leHRcbiAgICB9XG4gICAgcmV0dXJuIHBcbiAgfVxuICBkZWxldGUoeCl7XG4gICAgbGV0IHAgPSB0aGlzLmZpbmRQcmV2aW91cyh4KVxuICAgIGlmKHApe1xuICAgICAgIGxldCBjdXIgPSBwLm5leHRcbiAgICAgICBwLm5leHQgPSBjdXIubmV4dFxuICAgIH1cbiAgfVxufSJdfQ==