'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.binarySearch = binarySearch;
function binarySearch(arr, item) {
  let low = 0,
      high = arr.length - 1,
      mid = 0,
      element = '';
  while (low <= high) {
    mid = Math.floor((low + high) / 2);
    element = arr[mid];
    if (element > item) {
      high = mid - 1;
    } else if (element < item) {
      low = mid + 1;
    } else {
      return mid;
    }
  }
  return -1;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL21vdWRsZS9hbGdvcml0aG1zL3NlYXJjaC9iaW5hcnkuanMiXSwibmFtZXMiOlsiYmluYXJ5U2VhcmNoIiwiYXJyIiwiaXRlbSIsImxvdyIsImhpZ2giLCJsZW5ndGgiLCJtaWQiLCJlbGVtZW50IiwiTWF0aCIsImZsb29yIl0sIm1hcHBpbmdzIjoiOzs7OztRQUFnQkEsWSxHQUFBQSxZO0FBQVQsU0FBU0EsWUFBVCxDQUFzQkMsR0FBdEIsRUFBMEJDLElBQTFCLEVBQStCO0FBQ25DLE1BQUlDLE1BQUssQ0FBVDtBQUFBLE1BQ0lDLE9BQU9ILElBQUlJLE1BQUosR0FBYSxDQUR4QjtBQUFBLE1BRUlDLE1BQU0sQ0FGVjtBQUFBLE1BRVlDLFVBQVUsRUFGdEI7QUFHQyxTQUFNSixPQUFLQyxJQUFYLEVBQWdCO0FBQ2RFLFVBQU1FLEtBQUtDLEtBQUwsQ0FBVyxDQUFDTixNQUFJQyxJQUFMLElBQVcsQ0FBdEIsQ0FBTjtBQUNBRyxjQUFVTixJQUFJSyxHQUFKLENBQVY7QUFDQSxRQUFHQyxVQUFVTCxJQUFiLEVBQWtCO0FBQ2hCRSxhQUFPRSxNQUFNLENBQWI7QUFDRCxLQUZELE1BRU0sSUFBR0MsVUFBVUwsSUFBYixFQUFrQjtBQUN0QkMsWUFBTUcsTUFBTSxDQUFaO0FBQ0QsS0FGSyxNQUVEO0FBQ0gsYUFBT0EsR0FBUDtBQUNEO0FBQ0Y7QUFDSCxTQUFPLENBQUMsQ0FBUjtBQUNEIiwiZmlsZSI6ImJpbmFyeS5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiBiaW5hcnlTZWFyY2goYXJyLGl0ZW0pe1xuICAgbGV0IGxvdyA9MCxcbiAgICAgICBoaWdoID0gYXJyLmxlbmd0aCAtIDEsXG4gICAgICAgbWlkID0gMCxlbGVtZW50ID0gJydcbiAgICB3aGlsZShsb3c8PWhpZ2gpe1xuICAgICAgbWlkID0gTWF0aC5mbG9vcigobG93K2hpZ2gpLzIpXG4gICAgICBlbGVtZW50ID0gYXJyW21pZF1cbiAgICAgIGlmKGVsZW1lbnQgPiBpdGVtKXtcbiAgICAgICAgaGlnaCA9IG1pZCAtIDFcbiAgICAgIH1lbHNlIGlmKGVsZW1lbnQgPCBpdGVtKXtcbiAgICAgICAgbG93ID0gbWlkICsgMVxuICAgICAgfWVsc2V7XG4gICAgICAgIHJldHVybiBtaWRcbiAgICAgIH1cbiAgICB9XG4gIHJldHVybiAtMSBcbn0iXX0=