"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.qiuckSort = qiuckSort;

const qiuck = (arr, i, n) => {
  let index = 0;
  if (arr.length > 1) {
    index = partition(arr, i, n);
    if (i < index - 1) {
      qiuck(arr, i, index - 1);
    }
    if (n > index + 1) {
      qiuck(arr, index, n);
    }
  }
};

const partition = (arr, left, right) => {
  let pivot = arr[Math.floor((left + right) / 2)],
      i = left,
      j = right;
  while (i <= j) {
    while (arr[i] < pivot) {
      i++;
    }
    while (arr[j] > pivot) {
      j--;
    }
    if (i <= j) {
      let mid = arr[i];
      arr[i] = arr[j];
      arr[j] = mid;
      i++;
      j--;
    }
  }
  return i;
};

function qiuckSort(arr, i, n) {
  return qiuck(arr, i, n);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL21vdWRsZS9hbGdvcml0aG1zL3NvcnQvcWl1Y2suanMiXSwibmFtZXMiOlsicWl1Y2tTb3J0IiwicWl1Y2siLCJhcnIiLCJpIiwibiIsImluZGV4IiwibGVuZ3RoIiwicGFydGl0aW9uIiwibGVmdCIsInJpZ2h0IiwicGl2b3QiLCJNYXRoIiwiZmxvb3IiLCJqIiwibWlkIl0sIm1hcHBpbmdzIjoiOzs7OztRQXdDZ0JBLFMsR0FBQUEsUzs7QUF2Q2hCLE1BQU1DLFFBQVEsQ0FBQ0MsR0FBRCxFQUFLQyxDQUFMLEVBQU9DLENBQVAsS0FBVztBQUNyQixNQUFJQyxRQUFRLENBQVo7QUFDQSxNQUFHSCxJQUFJSSxNQUFKLEdBQVcsQ0FBZCxFQUFnQjtBQUNaRCxZQUFRRSxVQUFVTCxHQUFWLEVBQWNDLENBQWQsRUFBZ0JDLENBQWhCLENBQVI7QUFDQSxRQUFHRCxJQUFFRSxRQUFNLENBQVgsRUFBYTtBQUNYSixZQUFNQyxHQUFOLEVBQVVDLENBQVYsRUFBWUUsUUFBTSxDQUFsQjtBQUNEO0FBQ0QsUUFBR0QsSUFBRUMsUUFBTSxDQUFYLEVBQWE7QUFDWEosWUFBTUMsR0FBTixFQUFVRyxLQUFWLEVBQWdCRCxDQUFoQjtBQUNEO0FBQ0o7QUFDSixDQVhEOztBQWFBLE1BQU1HLFlBQVksQ0FBQ0wsR0FBRCxFQUFLTSxJQUFMLEVBQVVDLEtBQVYsS0FBa0I7QUFDaEMsTUFBSUMsUUFBUVIsSUFBSVMsS0FBS0MsS0FBTCxDQUFXLENBQUNKLE9BQUtDLEtBQU4sSUFBYSxDQUF4QixDQUFKLENBQVo7QUFBQSxNQUNJTixJQUFJSyxJQURSO0FBQUEsTUFDZUssSUFBSUosS0FEbkI7QUFFQSxTQUFNTixLQUFHVSxDQUFULEVBQVc7QUFDVCxXQUFNWCxJQUFJQyxDQUFKLElBQU9PLEtBQWIsRUFBbUI7QUFDakJQO0FBQ0Q7QUFDRCxXQUFNRCxJQUFJVyxDQUFKLElBQU9ILEtBQWIsRUFBbUI7QUFDakJHO0FBQ0Q7QUFDRCxRQUFHVixLQUFHVSxDQUFOLEVBQVE7QUFDTCxVQUFJQyxNQUFNWixJQUFJQyxDQUFKLENBQVY7QUFDQUQsVUFBSUMsQ0FBSixJQUFTRCxJQUFJVyxDQUFKLENBQVQ7QUFDQVgsVUFBSVcsQ0FBSixJQUFTQyxHQUFUO0FBQ0FYO0FBQ0FVO0FBQ0Y7QUFDRjtBQUNELFNBQU9WLENBQVA7QUFDSCxDQW5CRDs7QUEwQk8sU0FBU0gsU0FBVCxDQUFtQkUsR0FBbkIsRUFBdUJDLENBQXZCLEVBQXlCQyxDQUF6QixFQUEyQjtBQUMvQixTQUFPSCxNQUFNQyxHQUFOLEVBQVVDLENBQVYsRUFBWUMsQ0FBWixDQUFQO0FBQ0YiLCJmaWxlIjoicWl1Y2suanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmNvbnN0IHFpdWNrID0gKGFycixpLG4pPT57XG4gICAgbGV0IGluZGV4ID0gMFxuICAgIGlmKGFyci5sZW5ndGg+MSl7XG4gICAgICAgIGluZGV4ID0gcGFydGl0aW9uKGFycixpLG4pXG4gICAgICAgIGlmKGk8aW5kZXgtMSl7XG4gICAgICAgICAgcWl1Y2soYXJyLGksaW5kZXgtMSlcbiAgICAgICAgfVxuICAgICAgICBpZihuPmluZGV4KzEpe1xuICAgICAgICAgIHFpdWNrKGFycixpbmRleCxuKVxuICAgICAgICB9XG4gICAgfVxufVxuXG5jb25zdCBwYXJ0aXRpb24gPSAoYXJyLGxlZnQscmlnaHQpPT57XG4gICAgbGV0IHBpdm90ID0gYXJyW01hdGguZmxvb3IoKGxlZnQrcmlnaHQpLzIpXSxcbiAgICAgICAgaSA9IGxlZnQgLCBqID0gcmlnaHRcbiAgICB3aGlsZShpPD1qKXtcbiAgICAgIHdoaWxlKGFycltpXTxwaXZvdCl7XG4gICAgICAgIGkrK1xuICAgICAgfVxuICAgICAgd2hpbGUoYXJyW2pdPnBpdm90KXtcbiAgICAgICAgai0tXG4gICAgICB9XG4gICAgICBpZihpPD1qKXtcbiAgICAgICAgIGxldCBtaWQgPSBhcnJbaV1cbiAgICAgICAgIGFycltpXSA9IGFycltqXVxuICAgICAgICAgYXJyW2pdID0gbWlkXG4gICAgICAgICBpKytcbiAgICAgICAgIGotLVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gaVxufVxuXG5cblxuXG5cblxuZXhwb3J0IGZ1bmN0aW9uIHFpdWNrU29ydChhcnIsaSxuKXtcbiAgIHJldHVybiBxaXVjayhhcnIsaSxuKVxufVxuIl19