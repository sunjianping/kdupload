'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _chainTable = require('./chainTable');

var _chainTable2 = _interopRequireDefault(_chainTable);

var _nextPrime = require('../runtime/nextPrime');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class hashTree {
  constructor(size) {
    this.tableSize = (0, _nextPrime.nextPrime)(size);
    console.log(this.tableSize);
    this.table = new Array(this.tableSize).fill(0);
  }
  hash(x) {
    let key = x + '',
        i = 0,
        len = key.length,
        hashVal = 0;
    while (len !== i) {
      hashVal = (hashVal << 5) + key[i++];
    }
    return hashVal % this.tableSize;
  }
  insert(x) {
    let hash = this.hash(x);
    let ctable = this.table[hash];
    if (!ctable) {
      ctable = new _chainTable2.default();
      this.table[hash] = ctable;
    }

    ctable.insert(x);
  }
  find(x) {
    let ctable = this.table[this.hash(x)];
    return ctable.find(x);
  }
}
exports.default = hashTree;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vdWRsZS9hZHQvaGFzaFRhYmxlLmpzIl0sIm5hbWVzIjpbImhhc2hUcmVlIiwiY29uc3RydWN0b3IiLCJzaXplIiwidGFibGVTaXplIiwiY29uc29sZSIsImxvZyIsInRhYmxlIiwiQXJyYXkiLCJmaWxsIiwiaGFzaCIsIngiLCJrZXkiLCJpIiwibGVuIiwibGVuZ3RoIiwiaGFzaFZhbCIsImluc2VydCIsImN0YWJsZSIsImZpbmQiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOzs7O0FBQ0E7Ozs7QUFDZSxNQUFNQSxRQUFOLENBQWM7QUFDM0JDLGNBQVlDLElBQVosRUFBaUI7QUFDZixTQUFLQyxTQUFMLEdBQWlCLDBCQUFVRCxJQUFWLENBQWpCO0FBQ0FFLFlBQVFDLEdBQVIsQ0FBWSxLQUFLRixTQUFqQjtBQUNBLFNBQUtHLEtBQUwsR0FBYSxJQUFJQyxLQUFKLENBQVUsS0FBS0osU0FBZixFQUEwQkssSUFBMUIsQ0FBK0IsQ0FBL0IsQ0FBYjtBQUNEO0FBQ0RDLE9BQUtDLENBQUwsRUFBTztBQUNMLFFBQUlDLE1BQU1ELElBQUUsRUFBWjtBQUFBLFFBQWVFLElBQUksQ0FBbkI7QUFBQSxRQUFxQkMsTUFBTUYsSUFBSUcsTUFBL0I7QUFBQSxRQUFzQ0MsVUFBVSxDQUFoRDtBQUNBLFdBQU1GLFFBQU1ELENBQVosRUFBYztBQUNaRyxnQkFBVSxDQUFDQSxXQUFTLENBQVYsSUFBZUosSUFBSUMsR0FBSixDQUF6QjtBQUNEO0FBQ0QsV0FBT0csVUFBVSxLQUFLWixTQUF0QjtBQUNEO0FBQ0RhLFNBQU9OLENBQVAsRUFBUztBQUNQLFFBQUlELE9BQU8sS0FBS0EsSUFBTCxDQUFVQyxDQUFWLENBQVg7QUFDQSxRQUFJTyxTQUFTLEtBQUtYLEtBQUwsQ0FBV0csSUFBWCxDQUFiO0FBQ0EsUUFBRyxDQUFDUSxNQUFKLEVBQVc7QUFDVEEsZUFBUywwQkFBVDtBQUNBLFdBQUtYLEtBQUwsQ0FBV0csSUFBWCxJQUFtQlEsTUFBbkI7QUFDRDs7QUFFREEsV0FBT0QsTUFBUCxDQUFjTixDQUFkO0FBQ0Q7QUFDRFEsT0FBS1IsQ0FBTCxFQUFPO0FBQ0wsUUFBSU8sU0FBUyxLQUFLWCxLQUFMLENBQVcsS0FBS0csSUFBTCxDQUFVQyxDQUFWLENBQVgsQ0FBYjtBQUNBLFdBQU9PLE9BQU9DLElBQVAsQ0FBWVIsQ0FBWixDQUFQO0FBQ0Q7QUExQjBCO2tCQUFSVixRIiwiZmlsZSI6Imhhc2hUYWJsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBjaGFpblRhYmxlIGZyb20gJy4vY2hhaW5UYWJsZSdcbmltcG9ydCB7bmV4dFByaW1lfSBmcm9tICcuLi9ydW50aW1lL25leHRQcmltZSdcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGhhc2hUcmVle1xuICBjb25zdHJ1Y3RvcihzaXplKXtcbiAgICB0aGlzLnRhYmxlU2l6ZSA9IG5leHRQcmltZShzaXplKVxuICAgIGNvbnNvbGUubG9nKHRoaXMudGFibGVTaXplKVxuICAgIHRoaXMudGFibGUgPSBuZXcgQXJyYXkodGhpcy50YWJsZVNpemUpLmZpbGwoMCkgXG4gIH1cbiAgaGFzaCh4KXtcbiAgICBsZXQga2V5ID0geCsnJyxpID0gMCxsZW4gPSBrZXkubGVuZ3RoLGhhc2hWYWwgPSAwXG4gICAgd2hpbGUobGVuIT09aSl7XG4gICAgICBoYXNoVmFsID0gKGhhc2hWYWw8PDUpICsga2V5W2krK11cbiAgICB9XG4gICAgcmV0dXJuIGhhc2hWYWwgJSB0aGlzLnRhYmxlU2l6ZVxuICB9XG4gIGluc2VydCh4KXtcbiAgICBsZXQgaGFzaCA9IHRoaXMuaGFzaCh4KVxuICAgIGxldCBjdGFibGUgPSB0aGlzLnRhYmxlW2hhc2hdXG4gICAgaWYoIWN0YWJsZSl7XG4gICAgICBjdGFibGUgPSBuZXcgY2hhaW5UYWJsZSgpXG4gICAgICB0aGlzLnRhYmxlW2hhc2hdID0gY3RhYmxlXG4gICAgfVxuICAgIFxuICAgIGN0YWJsZS5pbnNlcnQoeClcbiAgfVxuICBmaW5kKHgpe1xuICAgIGxldCBjdGFibGUgPSB0aGlzLnRhYmxlW3RoaXMuaGFzaCh4KV1cbiAgICByZXR1cm4gY3RhYmxlLmZpbmQoeClcbiAgfVxufSJdfQ==