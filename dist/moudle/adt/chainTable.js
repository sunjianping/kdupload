'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

const node = {
  element: '',
  next: ''
};
class chainTable {
  constructor() {
    this.header = _extends({}, node);
  }
  insert(x) {
    let p = this.header;
    while (p.element !== '' && p.element !== x) {
      if (p.next === '') {
        p.next = _extends({}, node);
      }
      p = p.next;
    }
    p.element = x;
  }
  find(x) {
    let p = this.header;
    while (p.element !== x && p.next !== '') {
      p = p.next;
    }
    return p;
  }
  findPrevious(x) {
    let p = this.header;
    while (p.next.element !== x && p.next !== '') {
      p = p.next;
    }
    return p;
  }
  delete(x) {
    let p = this.findPrevious(x);
    if (p) {
      let cur = p.next;
      p.next = cur.next;
    }
  }
}
exports.default = chainTable;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vdWRsZS9hZHQvY2hhaW5UYWJsZS5qcyJdLCJuYW1lcyI6WyJub2RlIiwiZWxlbWVudCIsIm5leHQiLCJjaGFpblRhYmxlIiwiY29uc3RydWN0b3IiLCJoZWFkZXIiLCJpbnNlcnQiLCJ4IiwicCIsImZpbmQiLCJmaW5kUHJldmlvdXMiLCJkZWxldGUiLCJjdXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQ0EsTUFBTUEsT0FBTztBQUNYQyxXQUFRLEVBREc7QUFFWEMsUUFBSztBQUZNLENBQWI7QUFJZSxNQUFNQyxVQUFOLENBQWdCO0FBQzdCQyxnQkFBYTtBQUNYLFNBQUtDLE1BQUwsZ0JBQWtCTCxJQUFsQjtBQUNEO0FBQ0RNLFNBQU9DLENBQVAsRUFBUztBQUNQLFFBQUlDLElBQUksS0FBS0gsTUFBYjtBQUNBLFdBQU1HLEVBQUVQLE9BQUYsS0FBWSxFQUFaLElBQWdCTyxFQUFFUCxPQUFGLEtBQVlNLENBQWxDLEVBQW9DO0FBQ2pDLFVBQUdDLEVBQUVOLElBQUYsS0FBUyxFQUFaLEVBQWU7QUFDYk0sVUFBRU4sSUFBRixnQkFBYUYsSUFBYjtBQUNEO0FBQ0RRLFVBQUlBLEVBQUVOLElBQU47QUFDRjtBQUNETSxNQUFFUCxPQUFGLEdBQVlNLENBQVo7QUFDRDtBQUNERSxPQUFLRixDQUFMLEVBQU87QUFDTCxRQUFJQyxJQUFJLEtBQUtILE1BQWI7QUFDQSxXQUFNRyxFQUFFUCxPQUFGLEtBQVlNLENBQVosSUFBZUMsRUFBRU4sSUFBRixLQUFTLEVBQTlCLEVBQWlDO0FBQy9CTSxVQUFJQSxFQUFFTixJQUFOO0FBQ0Q7QUFDRCxXQUFPTSxDQUFQO0FBQ0Q7QUFDREUsZUFBYUgsQ0FBYixFQUFlO0FBQ2IsUUFBSUMsSUFBSSxLQUFLSCxNQUFiO0FBQ0EsV0FBTUcsRUFBRU4sSUFBRixDQUFPRCxPQUFQLEtBQWlCTSxDQUFqQixJQUFvQkMsRUFBRU4sSUFBRixLQUFTLEVBQW5DLEVBQXNDO0FBQ3BDTSxVQUFJQSxFQUFFTixJQUFOO0FBQ0Q7QUFDRCxXQUFPTSxDQUFQO0FBQ0Q7QUFDREcsU0FBT0osQ0FBUCxFQUFTO0FBQ1AsUUFBSUMsSUFBSSxLQUFLRSxZQUFMLENBQWtCSCxDQUFsQixDQUFSO0FBQ0EsUUFBR0MsQ0FBSCxFQUFLO0FBQ0YsVUFBSUksTUFBTUosRUFBRU4sSUFBWjtBQUNBTSxRQUFFTixJQUFGLEdBQVNVLElBQUlWLElBQWI7QUFDRjtBQUNGO0FBbEM0QjtrQkFBVkMsVSIsImZpbGUiOiJjaGFpblRhYmxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5jb25zdCBub2RlID0ge1xuICBlbGVtZW50OicnLFxuICBuZXh0OicnXG59XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBjaGFpblRhYmxle1xuICBjb25zdHJ1Y3Rvcigpe1xuICAgIHRoaXMuaGVhZGVyID0gey4uLm5vZGV9XG4gIH1cbiAgaW5zZXJ0KHgpe1xuICAgIGxldCBwID0gdGhpcy5oZWFkZXJcbiAgICB3aGlsZShwLmVsZW1lbnQhPT0nJyYmcC5lbGVtZW50IT09eCl7XG4gICAgICAgaWYocC5uZXh0PT09Jycpe1xuICAgICAgICAgcC5uZXh0ID0gey4uLm5vZGV9XG4gICAgICAgfVxuICAgICAgIHAgPSBwLm5leHRcbiAgICB9XG4gICAgcC5lbGVtZW50ID0geFxuICB9XG4gIGZpbmQoeCl7XG4gICAgbGV0IHAgPSB0aGlzLmhlYWRlclxuICAgIHdoaWxlKHAuZWxlbWVudCE9PXgmJnAubmV4dCE9PScnKXtcbiAgICAgIHAgPSBwLm5leHRcbiAgICB9XG4gICAgcmV0dXJuIHBcbiAgfVxuICBmaW5kUHJldmlvdXMoeCl7XG4gICAgbGV0IHAgPSB0aGlzLmhlYWRlclxuICAgIHdoaWxlKHAubmV4dC5lbGVtZW50IT09eCYmcC5uZXh0IT09Jycpe1xuICAgICAgcCA9IHAubmV4dFxuICAgIH1cbiAgICByZXR1cm4gcFxuICB9XG4gIGRlbGV0ZSh4KXtcbiAgICBsZXQgcCA9IHRoaXMuZmluZFByZXZpb3VzKHgpXG4gICAgaWYocCl7XG4gICAgICAgbGV0IGN1ciA9IHAubmV4dFxuICAgICAgIHAubmV4dCA9IGN1ci5uZXh0XG4gICAgfVxuICB9XG59Il19