'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _blueimpMd = require('blueimp-md5');

var _blueimpMd2 = _interopRequireDefault(_blueimpMd);

var _priorityQueue = require('./runtime/priorityQueue');

var _priorityQueue2 = _interopRequireDefault(_priorityQueue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class KdPanUploader {

    constructor(options = {}) {
        this.options = options;
        this.priorityQueue = new _priorityQueue2.default();
        this.priorityQueue.enqueue(this.kd_md5('hello'), 1);
        this.priorityQueue.enqueue(this.kd_md5('sunjianping'), 0);
        this.priorityQueue.print();
    }
    kd_md5(string) {
        const md5obj = (0, _blueimpMd2.default)(string);
        return md5obj;
    }
}
exports.default = KdPanUploader;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL21vdWRsZS9rZFBhblVwbG9hZGVyLmpzIl0sIm5hbWVzIjpbIktkUGFuVXBsb2FkZXIiLCJjb25zdHJ1Y3RvciIsIm9wdGlvbnMiLCJwcmlvcml0eVF1ZXVlIiwiZW5xdWV1ZSIsImtkX21kNSIsInByaW50Iiwic3RyaW5nIiwibWQ1b2JqIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTs7OztBQUNBOzs7Ozs7QUFDZSxNQUFNQSxhQUFOLENBQW9COztBQUUvQkMsZ0JBQWFDLFVBQVUsRUFBdkIsRUFBMEI7QUFDdkIsYUFBS0EsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsYUFBS0MsYUFBTCxHQUFxQiw2QkFBckI7QUFDQSxhQUFLQSxhQUFMLENBQW1CQyxPQUFuQixDQUEyQixLQUFLQyxNQUFMLENBQVksT0FBWixDQUEzQixFQUFnRCxDQUFoRDtBQUNBLGFBQUtGLGFBQUwsQ0FBbUJDLE9BQW5CLENBQTJCLEtBQUtDLE1BQUwsQ0FBWSxhQUFaLENBQTNCLEVBQXNELENBQXREO0FBQ0EsYUFBS0YsYUFBTCxDQUFtQkcsS0FBbkI7QUFDRjtBQUNERCxXQUFPRSxNQUFQLEVBQWM7QUFDVixjQUFNQyxTQUFTLHlCQUFJRCxNQUFKLENBQWY7QUFDQSxlQUFPQyxNQUFQO0FBQ0g7QUFaOEI7a0JBQWRSLGEiLCJmaWxlIjoia2RQYW5VcGxvYWRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBtZDUgZnJvbSAnYmx1ZWltcC1tZDUnXG5pbXBvcnQgUHJpb3JpdHlRdWV1ZSBmcm9tICcuL3J1bnRpbWUvcHJpb3JpdHlRdWV1ZSdcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEtkUGFuVXBsb2FkZXIge1xuXG4gICAgY29uc3RydWN0b3IgKG9wdGlvbnMgPSB7fSl7XG4gICAgICAgdGhpcy5vcHRpb25zID0gb3B0aW9uc1xuICAgICAgIHRoaXMucHJpb3JpdHlRdWV1ZSA9IG5ldyBQcmlvcml0eVF1ZXVlKClcbiAgICAgICB0aGlzLnByaW9yaXR5UXVldWUuZW5xdWV1ZSh0aGlzLmtkX21kNSgnaGVsbG8nKSwxKVxuICAgICAgIHRoaXMucHJpb3JpdHlRdWV1ZS5lbnF1ZXVlKHRoaXMua2RfbWQ1KCdzdW5qaWFucGluZycpLDApXG4gICAgICAgdGhpcy5wcmlvcml0eVF1ZXVlLnByaW50KClcbiAgICB9XG4gICAga2RfbWQ1KHN0cmluZyl7XG4gICAgICAgIGNvbnN0IG1kNW9iaiA9IG1kNShzdHJpbmcpXG4gICAgICAgIHJldHVybiBtZDVvYmpcbiAgICB9XG59Il19