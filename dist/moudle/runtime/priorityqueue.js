'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _queue = require('./queue');

var _queue2 = _interopRequireDefault(_queue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class PriorityQueue extends _queue2.default {
  constructor() {
    super();
  }
  enqueue(element, priority) {
    const queueElement = this.queueElement(element, priority);
    if (this.isEmpty()) {
      this.items.push(queueElement);
    } else {
      var added = false;
      for (let i = 0; i < this.items.length; i++) {
        if (queueElement.priority < this.items[i].priority) {
          this.items.splice(i, 0, queueElement);
          added = true;
          break;
        }
      }
      if (!added) {
        this.items.push(queueElement);
      }
    }
  }
  queueElement(element = '', priority = 0) {
    return {
      element: element,
      priority: priority
    };
  }
  print() {
    var printv = [];
    for (let i = 0; i < this.items.length; i++) {
      printv.push(this.items[i].element);
    }
  }
}
exports.default = PriorityQueue;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vdWRsZS9ydW50aW1lL3ByaW9yaXR5cXVldWUuanMiXSwibmFtZXMiOlsiUHJpb3JpdHlRdWV1ZSIsImNvbnN0cnVjdG9yIiwiZW5xdWV1ZSIsImVsZW1lbnQiLCJwcmlvcml0eSIsInF1ZXVlRWxlbWVudCIsImlzRW1wdHkiLCJpdGVtcyIsInB1c2giLCJhZGRlZCIsImkiLCJsZW5ndGgiLCJzcGxpY2UiLCJwcmludCIsInByaW50diJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7Ozs7OztBQUVlLE1BQU1BLGFBQU4seUJBQWtDO0FBQy9DQyxnQkFBYTtBQUNYO0FBQ0Q7QUFDREMsVUFBUUMsT0FBUixFQUFnQkMsUUFBaEIsRUFBeUI7QUFDdkIsVUFBTUMsZUFBZSxLQUFLQSxZQUFMLENBQWtCRixPQUFsQixFQUEwQkMsUUFBMUIsQ0FBckI7QUFDQSxRQUFHLEtBQUtFLE9BQUwsRUFBSCxFQUFrQjtBQUNoQixXQUFLQyxLQUFMLENBQVdDLElBQVgsQ0FBZ0JILFlBQWhCO0FBQ0QsS0FGRCxNQUVLO0FBQ0gsVUFBSUksUUFBUSxLQUFaO0FBQ0EsV0FBSSxJQUFJQyxJQUFJLENBQVosRUFBZUEsSUFBRSxLQUFLSCxLQUFMLENBQVdJLE1BQTVCLEVBQW9DRCxHQUFwQyxFQUF3QztBQUN0QyxZQUFHTCxhQUFhRCxRQUFiLEdBQXdCLEtBQUtHLEtBQUwsQ0FBV0csQ0FBWCxFQUFjTixRQUF6QyxFQUFrRDtBQUNoRCxlQUFLRyxLQUFMLENBQVdLLE1BQVgsQ0FBa0JGLENBQWxCLEVBQW9CLENBQXBCLEVBQXNCTCxZQUF0QjtBQUNBSSxrQkFBUSxJQUFSO0FBQ0E7QUFDRDtBQUNGO0FBQ0QsVUFBRyxDQUFDQSxLQUFKLEVBQVU7QUFDUixhQUFLRixLQUFMLENBQVdDLElBQVgsQ0FBZ0JILFlBQWhCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0RBLGVBQWFGLFVBQVUsRUFBdkIsRUFBMEJDLFdBQVcsQ0FBckMsRUFBdUM7QUFDckMsV0FBTztBQUNMRCxlQUFRQSxPQURIO0FBRUxDLGdCQUFTQTtBQUZKLEtBQVA7QUFJRDtBQUNEUyxVQUFPO0FBQ0wsUUFBSUMsU0FBUyxFQUFiO0FBQ0EsU0FBSSxJQUFJSixJQUFJLENBQVosRUFBY0EsSUFBRSxLQUFLSCxLQUFMLENBQVdJLE1BQTNCLEVBQWtDRCxHQUFsQyxFQUFzQztBQUNwQ0ksYUFBT04sSUFBUCxDQUFZLEtBQUtELEtBQUwsQ0FBV0csQ0FBWCxFQUFjUCxPQUExQjtBQUNEO0FBQ0Y7QUFqQzhDO2tCQUE1QkgsYSIsImZpbGUiOiJwcmlvcml0eXF1ZXVlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFF1ZXVlIGZyb20gJy4vcXVldWUnXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFByaW9yaXR5UXVldWUgZXh0ZW5kcyBRdWV1ZSB7XG4gIGNvbnN0cnVjdG9yKCl7XG4gICAgc3VwZXIoKVxuICB9XG4gIGVucXVldWUoZWxlbWVudCxwcmlvcml0eSl7XG4gICAgY29uc3QgcXVldWVFbGVtZW50ID0gdGhpcy5xdWV1ZUVsZW1lbnQoZWxlbWVudCxwcmlvcml0eSlcbiAgICBpZih0aGlzLmlzRW1wdHkoKSl7XG4gICAgICB0aGlzLml0ZW1zLnB1c2gocXVldWVFbGVtZW50KVxuICAgIH1lbHNle1xuICAgICAgdmFyIGFkZGVkID0gZmFsc2VcbiAgICAgIGZvcihsZXQgaSA9IDA7IGk8dGhpcy5pdGVtcy5sZW5ndGg7IGkrKyl7XG4gICAgICAgIGlmKHF1ZXVlRWxlbWVudC5wcmlvcml0eSA8IHRoaXMuaXRlbXNbaV0ucHJpb3JpdHkpe1xuICAgICAgICAgIHRoaXMuaXRlbXMuc3BsaWNlKGksMCxxdWV1ZUVsZW1lbnQpXG4gICAgICAgICAgYWRkZWQgPSB0cnVlXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYoIWFkZGVkKXtcbiAgICAgICAgdGhpcy5pdGVtcy5wdXNoKHF1ZXVlRWxlbWVudClcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcXVldWVFbGVtZW50KGVsZW1lbnQgPSAnJyxwcmlvcml0eSA9IDApe1xuICAgIHJldHVybiB7XG4gICAgICBlbGVtZW50OmVsZW1lbnQsXG4gICAgICBwcmlvcml0eTpwcmlvcml0eVxuICAgIH1cbiAgfVxuICBwcmludCgpe1xuICAgIHZhciBwcmludHYgPSBbXVxuICAgIGZvcihsZXQgaSA9IDA7aTx0aGlzLml0ZW1zLmxlbmd0aDtpKyspe1xuICAgICAgcHJpbnR2LnB1c2godGhpcy5pdGVtc1tpXS5lbGVtZW50KVxuICAgIH1cbiAgfVxufSJdfQ==