"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
class Queue {
  constructor() {
    this.items = [];
  }
  enqueue(element) {
    this.items.push(element);
  }
  dequeue() {
    return this.items.shift();
  }
  front() {
    return this.items[0];
  }
  isEmpty() {
    return this.items.length == 0;
  }
  size() {
    return this.items.length;
  }
  clear() {
    this.items = [];
  }
  print() {
    console.log(this.items.toString());
  }
}
exports.default = Queue;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vdWRsZS9ydW50aW1lL3F1ZXVlLmpzIl0sIm5hbWVzIjpbIlF1ZXVlIiwiY29uc3RydWN0b3IiLCJpdGVtcyIsImVucXVldWUiLCJlbGVtZW50IiwicHVzaCIsImRlcXVldWUiLCJzaGlmdCIsImZyb250IiwiaXNFbXB0eSIsImxlbmd0aCIsInNpemUiLCJjbGVhciIsInByaW50IiwiY29uc29sZSIsImxvZyIsInRvU3RyaW5nIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFlLE1BQU1BLEtBQU4sQ0FBWTtBQUN6QkMsZ0JBQWE7QUFDWCxTQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUNEO0FBQ0RDLFVBQVFDLE9BQVIsRUFBZ0I7QUFDZCxTQUFLRixLQUFMLENBQVdHLElBQVgsQ0FBZ0JELE9BQWhCO0FBQ0Q7QUFDREUsWUFBUztBQUNQLFdBQU8sS0FBS0osS0FBTCxDQUFXSyxLQUFYLEVBQVA7QUFDRDtBQUNEQyxVQUFPO0FBQ0wsV0FBTyxLQUFLTixLQUFMLENBQVcsQ0FBWCxDQUFQO0FBQ0Q7QUFDRE8sWUFBUztBQUNQLFdBQU8sS0FBS1AsS0FBTCxDQUFXUSxNQUFYLElBQXFCLENBQTVCO0FBQ0Q7QUFDREMsU0FBTTtBQUNKLFdBQU8sS0FBS1QsS0FBTCxDQUFXUSxNQUFsQjtBQUNEO0FBQ0RFLFVBQU87QUFDTCxTQUFLVixLQUFMLEdBQWEsRUFBYjtBQUNEO0FBQ0RXLFVBQU87QUFDTEMsWUFBUUMsR0FBUixDQUFZLEtBQUtiLEtBQUwsQ0FBV2MsUUFBWCxFQUFaO0FBQ0Q7QUF4QndCO2tCQUFOaEIsSyIsImZpbGUiOiJxdWV1ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGNsYXNzIFF1ZXVlIHtcbiAgY29uc3RydWN0b3IoKXtcbiAgICB0aGlzLml0ZW1zID0gW11cbiAgfVxuICBlbnF1ZXVlKGVsZW1lbnQpe1xuICAgIHRoaXMuaXRlbXMucHVzaChlbGVtZW50KVxuICB9XG4gIGRlcXVldWUoKXtcbiAgICByZXR1cm4gdGhpcy5pdGVtcy5zaGlmdCgpXG4gIH1cbiAgZnJvbnQoKXtcbiAgICByZXR1cm4gdGhpcy5pdGVtc1swXVxuICB9XG4gIGlzRW1wdHkoKXtcbiAgICByZXR1cm4gdGhpcy5pdGVtcy5sZW5ndGggPT0gMFxuICB9XG4gIHNpemUoKXtcbiAgICByZXR1cm4gdGhpcy5pdGVtcy5sZW5ndGhcbiAgfVxuICBjbGVhcigpe1xuICAgIHRoaXMuaXRlbXMgPSBbXVxuICB9XG4gIHByaW50KCl7XG4gICAgY29uc29sZS5sb2codGhpcy5pdGVtcy50b1N0cmluZygpKVxuICB9XG59Il19