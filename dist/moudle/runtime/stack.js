"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
class Stack {
  constructor() {
    this.items = [];
  }
  push(element) {
    this.items.push(element);
  }
  pop() {
    return this.items.pop();
  }
  peek() {
    return this.items[this.items.length - 1];
  }
  isEmpty() {
    return this.items.length == 0;
  }
  clear() {
    this.items = [];
  }
  size() {
    return this.items.length;
  }
  print() {
    console.log(this.items.toString());
  }
}
exports.default = Stack;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vdWRsZS9ydW50aW1lL3N0YWNrLmpzIl0sIm5hbWVzIjpbIlN0YWNrIiwiY29uc3RydWN0b3IiLCJpdGVtcyIsInB1c2giLCJlbGVtZW50IiwicG9wIiwicGVlayIsImxlbmd0aCIsImlzRW1wdHkiLCJjbGVhciIsInNpemUiLCJwcmludCIsImNvbnNvbGUiLCJsb2ciLCJ0b1N0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBZSxNQUFNQSxLQUFOLENBQVc7QUFDeEJDLGdCQUFhO0FBQ1gsU0FBS0MsS0FBTCxHQUFhLEVBQWI7QUFDRDtBQUNEQyxPQUFLQyxPQUFMLEVBQWE7QUFDWCxTQUFLRixLQUFMLENBQVdDLElBQVgsQ0FBZ0JDLE9BQWhCO0FBQ0Q7QUFDREMsUUFBSztBQUNILFdBQU8sS0FBS0gsS0FBTCxDQUFXRyxHQUFYLEVBQVA7QUFDRDtBQUNEQyxTQUFNO0FBQ0osV0FBTyxLQUFLSixLQUFMLENBQVcsS0FBS0EsS0FBTCxDQUFXSyxNQUFYLEdBQW9CLENBQS9CLENBQVA7QUFDRDtBQUNEQyxZQUFTO0FBQ1AsV0FBTyxLQUFLTixLQUFMLENBQVdLLE1BQVgsSUFBcUIsQ0FBNUI7QUFDRDtBQUNERSxVQUFPO0FBQ0wsU0FBS1AsS0FBTCxHQUFhLEVBQWI7QUFDRDtBQUNEUSxTQUFNO0FBQ0osV0FBTyxLQUFLUixLQUFMLENBQVdLLE1BQWxCO0FBQ0Q7QUFDREksVUFBTztBQUNMQyxZQUFRQyxHQUFSLENBQVksS0FBS1gsS0FBTCxDQUFXWSxRQUFYLEVBQVo7QUFDRDtBQXhCdUI7a0JBQUxkLEsiLCJmaWxlIjoic3RhY2suanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBjbGFzcyBTdGFja3tcbiAgY29uc3RydWN0b3IoKXtcbiAgICB0aGlzLml0ZW1zID0gW11cbiAgfVxuICBwdXNoKGVsZW1lbnQpe1xuICAgIHRoaXMuaXRlbXMucHVzaChlbGVtZW50KVxuICB9XG4gIHBvcCgpe1xuICAgIHJldHVybiB0aGlzLml0ZW1zLnBvcCgpXG4gIH1cbiAgcGVlaygpe1xuICAgIHJldHVybiB0aGlzLml0ZW1zW3RoaXMuaXRlbXMubGVuZ3RoIC0gMV1cbiAgfVxuICBpc0VtcHR5KCl7XG4gICAgcmV0dXJuIHRoaXMuaXRlbXMubGVuZ3RoID09IDBcbiAgfVxuICBjbGVhcigpe1xuICAgIHRoaXMuaXRlbXMgPSBbXVxuICB9XG4gIHNpemUoKXtcbiAgICByZXR1cm4gdGhpcy5pdGVtcy5sZW5ndGhcbiAgfVxuICBwcmludCgpe1xuICAgIGNvbnNvbGUubG9nKHRoaXMuaXRlbXMudG9TdHJpbmcoKSlcbiAgfVxufSJdfQ==