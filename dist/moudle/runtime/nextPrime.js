"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nextPrime = nextPrime;
function nextPrime(m) {
  let prime = m,
      found = false;
  while (!found) {
    let n = Math.sqrt(prime),
        i = 2;
    for (i = 2; i <= n; i++) {
      if (prime % i === 0) {
        break;
      }
    }
    if (i > n) {
      break;
    }
    prime++;
  }
  return prime;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vdWRsZS9ydW50aW1lL25leHRQcmltZS5qcyJdLCJuYW1lcyI6WyJuZXh0UHJpbWUiLCJtIiwicHJpbWUiLCJmb3VuZCIsIm4iLCJNYXRoIiwic3FydCIsImkiXSwibWFwcGluZ3MiOiI7Ozs7O1FBR2dCQSxTLEdBQUFBLFM7QUFBVCxTQUFTQSxTQUFULENBQW1CQyxDQUFuQixFQUFxQjtBQUMxQixNQUFJQyxRQUFRRCxDQUFaO0FBQUEsTUFBY0UsUUFBUSxLQUF0QjtBQUNBLFNBQU0sQ0FBQ0EsS0FBUCxFQUFhO0FBQ1gsUUFBSUMsSUFBSUMsS0FBS0MsSUFBTCxDQUFVSixLQUFWLENBQVI7QUFBQSxRQUF5QkssSUFBSSxDQUE3QjtBQUNBLFNBQUlBLElBQUUsQ0FBTixFQUFRQSxLQUFHSCxDQUFYLEVBQWFHLEdBQWIsRUFBaUI7QUFDZixVQUFHTCxRQUFNSyxDQUFOLEtBQVUsQ0FBYixFQUFlO0FBQ2I7QUFDRDtBQUNGO0FBQ0QsUUFBR0EsSUFBRUgsQ0FBTCxFQUFPO0FBQ0w7QUFDRDtBQUNERjtBQUNEO0FBQ0QsU0FBT0EsS0FBUDtBQUNEIiwiZmlsZSI6Im5leHRQcmltZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG5cbmV4cG9ydCBmdW5jdGlvbiBuZXh0UHJpbWUobSl7XG4gIGxldCBwcmltZSA9IG0sZm91bmQgPSBmYWxzZVxuICB3aGlsZSghZm91bmQpe1xuICAgIGxldCBuID0gTWF0aC5zcXJ0KHByaW1lKSxpID0gMlxuICAgIGZvcihpPTI7aTw9bjtpKyspe1xuICAgICAgaWYocHJpbWUlaT09PTApe1xuICAgICAgICBicmVha1xuICAgICAgfVxuICAgIH1cbiAgICBpZihpPm4pe1xuICAgICAgYnJlYWtcbiAgICB9XG4gICAgcHJpbWUrK1xuICB9XG4gIHJldHVybiBwcmltZVxufSJdfQ==