'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var tree = {
  element: '',
  left: '',
  right: '',
  height: 0
};
var TreeNode = _extends({}, tree);

var binaryTree = function () {
  function binaryTree() {
    _classCallCheck(this, binaryTree);
  }

  _createClass(binaryTree, [{
    key: 'getTree',
    value: function getTree() {
      return TreeNode;
    }
  }, {
    key: 'insert',
    value: function insert(x) {
      var T = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : TreeNode;

      if (T.element === '') {
        T.element = x;
        T.right = _extends({}, tree);
        T.left = _extends({}, tree);
      } else if (x < T.element) {
        T.left = this.insert(x, T.left);
      } else if (x > T.element) {
        T.right = this.insert(x, T.right);
      }
      return T;
    }
  }, {
    key: 'findMin',
    value: function findMin() {
      var T = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : TreeNode;

      if (T.left && T.left.element !== '') {
        return this.findMin(T.left);
      } else if (T.element !== '') {
        return T;
      } else {
        return false;
      }
    }
  }, {
    key: 'findMax',
    value: function findMax() {
      var T = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : TreeNode;

      if (T.right && T.right.element !== '') {
        return this.findMax(T.right);
      } else if (T.element !== '') {
        return T;
      } else {
        return false;
      }
    }
  }, {
    key: 'Delete',
    value: function Delete(x) {
      var T = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : TreeNode;

      if (T.element === '') {
        return false;
      } else if (x < T.element) {
        this.Delete(x, T.left);
      } else if (x > T.element) {
        this.Delete(x, T.right);
      } else if (T.right && T.left && T.left.element !== '' && T.right.element !== '') {
        var minT = this.findMin(T.right);
        T.element = minT.element;
        T.right = this.Delete(T.element, T.right);
      } else {
        var _minT = T;
        if (T.left.element === '') {
          T = T.right;
        } else if (T.right.element === '') {
          T = T.left;
        }
      }
      return T;
    }
  }, {
    key: 'print',
    value: function print() {
      var T = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : TreeNode;

      if (T.element !== '') {
        if (T.left.element !== '') {
          this.print(T.left);
        }
        console.log(T.element);
        if (T.right.element !== '') {
          this.print(T.right);
        }
      }
    }
  }, {
    key: 'makeEmpty',
    value: function makeEmpty() {}
  }]);

  return binaryTree;
}();

exports.default = binaryTree;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL21vZHVsZS9hZHQvYmluYXJ5VHJlZS5qcyJdLCJuYW1lcyI6WyJ0cmVlIiwiZWxlbWVudCIsImxlZnQiLCJyaWdodCIsImhlaWdodCIsIlRyZWVOb2RlIiwiYmluYXJ5VHJlZSIsIngiLCJUIiwiaW5zZXJ0IiwiZmluZE1pbiIsImZpbmRNYXgiLCJEZWxldGUiLCJtaW5UIiwicHJpbnQiLCJjb25zb2xlIiwibG9nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxJQUFNQSxPQUFPO0FBQ1ZDLFdBQVEsRUFERTtBQUVWQyxRQUFLLEVBRks7QUFHVkMsU0FBTSxFQUhJO0FBSVZDLFVBQU87QUFKRyxDQUFiO0FBTUEsSUFBSUMsd0JBQWVMLElBQWYsQ0FBSjs7SUFDcUJNLFU7QUFDbkIsd0JBQWE7QUFBQTtBQUVaOzs7OzhCQUNRO0FBQ1AsYUFBT0QsUUFBUDtBQUNEOzs7MkJBQ01FLEMsRUFBYTtBQUFBLFVBQVhDLENBQVcsdUVBQVRILFFBQVM7O0FBQ2xCLFVBQUdHLEVBQUVQLE9BQUYsS0FBWSxFQUFmLEVBQWtCO0FBQ2hCTyxVQUFFUCxPQUFGLEdBQVlNLENBQVo7QUFDQUMsVUFBRUwsS0FBRixnQkFBY0gsSUFBZDtBQUNBUSxVQUFFTixJQUFGLGdCQUFhRixJQUFiO0FBQ0QsT0FKRCxNQUlNLElBQUdPLElBQUVDLEVBQUVQLE9BQVAsRUFBZTtBQUNuQk8sVUFBRU4sSUFBRixHQUFTLEtBQUtPLE1BQUwsQ0FBWUYsQ0FBWixFQUFjQyxFQUFFTixJQUFoQixDQUFUO0FBQ0QsT0FGSyxNQUVBLElBQUdLLElBQUVDLEVBQUVQLE9BQVAsRUFBZTtBQUNuQk8sVUFBRUwsS0FBRixHQUFVLEtBQUtNLE1BQUwsQ0FBWUYsQ0FBWixFQUFjQyxFQUFFTCxLQUFoQixDQUFWO0FBQ0Q7QUFDRCxhQUFPSyxDQUFQO0FBQ0Q7Ozs4QkFDa0I7QUFBQSxVQUFYQSxDQUFXLHVFQUFUSCxRQUFTOztBQUNqQixVQUFHRyxFQUFFTixJQUFGLElBQVFNLEVBQUVOLElBQUYsQ0FBT0QsT0FBUCxLQUFpQixFQUE1QixFQUErQjtBQUM3QixlQUFPLEtBQUtTLE9BQUwsQ0FBYUYsRUFBRU4sSUFBZixDQUFQO0FBQ0QsT0FGRCxNQUVNLElBQUdNLEVBQUVQLE9BQUYsS0FBWSxFQUFmLEVBQWtCO0FBQ3RCLGVBQU9PLENBQVA7QUFDRCxPQUZLLE1BRUQ7QUFDSCxlQUFPLEtBQVA7QUFDRDtBQUNGOzs7OEJBQ2tCO0FBQUEsVUFBWEEsQ0FBVyx1RUFBVEgsUUFBUzs7QUFDakIsVUFBR0csRUFBRUwsS0FBRixJQUFTSyxFQUFFTCxLQUFGLENBQVFGLE9BQVIsS0FBa0IsRUFBOUIsRUFBaUM7QUFDL0IsZUFBTyxLQUFLVSxPQUFMLENBQWFILEVBQUVMLEtBQWYsQ0FBUDtBQUNELE9BRkQsTUFFTSxJQUFHSyxFQUFFUCxPQUFGLEtBQVksRUFBZixFQUFrQjtBQUN0QixlQUFPTyxDQUFQO0FBQ0QsT0FGSyxNQUVEO0FBQ0gsZUFBTyxLQUFQO0FBQ0Q7QUFDRjs7OzJCQUNNRCxDLEVBQWE7QUFBQSxVQUFYQyxDQUFXLHVFQUFUSCxRQUFTOztBQUNsQixVQUFHRyxFQUFFUCxPQUFGLEtBQWMsRUFBakIsRUFBb0I7QUFDbEIsZUFBTyxLQUFQO0FBQ0QsT0FGRCxNQUVNLElBQUdNLElBQUVDLEVBQUVQLE9BQVAsRUFBZTtBQUNuQixhQUFLVyxNQUFMLENBQVlMLENBQVosRUFBY0MsRUFBRU4sSUFBaEI7QUFDRCxPQUZLLE1BRUEsSUFBR0ssSUFBRUMsRUFBRVAsT0FBUCxFQUFlO0FBQ25CLGFBQUtXLE1BQUwsQ0FBWUwsQ0FBWixFQUFjQyxFQUFFTCxLQUFoQjtBQUNELE9BRkssTUFFQSxJQUFHSyxFQUFFTCxLQUFGLElBQVNLLEVBQUVOLElBQVgsSUFBaUJNLEVBQUVOLElBQUYsQ0FBT0QsT0FBUCxLQUFpQixFQUFsQyxJQUFzQ08sRUFBRUwsS0FBRixDQUFRRixPQUFSLEtBQWtCLEVBQTNELEVBQThEO0FBQ2xFLFlBQUlZLE9BQU8sS0FBS0gsT0FBTCxDQUFhRixFQUFFTCxLQUFmLENBQVg7QUFDQUssVUFBRVAsT0FBRixHQUFZWSxLQUFLWixPQUFqQjtBQUNBTyxVQUFFTCxLQUFGLEdBQVUsS0FBS1MsTUFBTCxDQUFZSixFQUFFUCxPQUFkLEVBQXNCTyxFQUFFTCxLQUF4QixDQUFWO0FBQ0QsT0FKSyxNQUlEO0FBQ0gsWUFBSVUsUUFBT0wsQ0FBWDtBQUNBLFlBQUdBLEVBQUVOLElBQUYsQ0FBT0QsT0FBUCxLQUFtQixFQUF0QixFQUF5QjtBQUN2Qk8sY0FBSUEsRUFBRUwsS0FBTjtBQUNELFNBRkQsTUFFTSxJQUFHSyxFQUFFTCxLQUFGLENBQVFGLE9BQVIsS0FBb0IsRUFBdkIsRUFBMEI7QUFDOUJPLGNBQUlBLEVBQUVOLElBQU47QUFDRDtBQUNGO0FBQ0QsYUFBT00sQ0FBUDtBQUNEOzs7NEJBQ2dCO0FBQUEsVUFBWEEsQ0FBVyx1RUFBVEgsUUFBUzs7QUFDZixVQUFHRyxFQUFFUCxPQUFGLEtBQVksRUFBZixFQUFrQjtBQUNoQixZQUFHTyxFQUFFTixJQUFGLENBQU9ELE9BQVAsS0FBaUIsRUFBcEIsRUFBdUI7QUFDckIsZUFBS2EsS0FBTCxDQUFXTixFQUFFTixJQUFiO0FBQ0Q7QUFDRGEsZ0JBQVFDLEdBQVIsQ0FBWVIsRUFBRVAsT0FBZDtBQUNBLFlBQUdPLEVBQUVMLEtBQUYsQ0FBUUYsT0FBUixLQUFrQixFQUFyQixFQUF3QjtBQUN0QixlQUFLYSxLQUFMLENBQVdOLEVBQUVMLEtBQWI7QUFDRDtBQUVGO0FBQ0Y7OztnQ0FDVSxDQUVWOzs7Ozs7a0JBeEVrQkcsVSIsImZpbGUiOiJiaW5hcnlUcmVlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgdHJlZSA9IHtcbiAgIGVsZW1lbnQ6JycsXG4gICBsZWZ0OicnLFxuICAgcmlnaHQ6JycsXG4gICBoZWlnaHQ6MFxufVxubGV0IFRyZWVOb2RlID0gey4uLnRyZWV9XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBiaW5hcnlUcmVlIHtcbiAgY29uc3RydWN0b3IoKXtcbiAgICBcbiAgfVxuICBnZXRUcmVlKCl7XG4gICAgcmV0dXJuIFRyZWVOb2RlXG4gIH1cbiAgaW5zZXJ0KHgsVD1UcmVlTm9kZSl7XG4gICAgaWYoVC5lbGVtZW50PT09Jycpe1xuICAgICAgVC5lbGVtZW50ID0geFxuICAgICAgVC5yaWdodCA9IHsuLi50cmVlfVxuICAgICAgVC5sZWZ0ID0gey4uLnRyZWV9XG4gICAgfWVsc2UgaWYoeDxULmVsZW1lbnQpe1xuICAgICAgVC5sZWZ0ID0gdGhpcy5pbnNlcnQoeCxULmxlZnQpXG4gICAgfWVsc2UgaWYoeD5ULmVsZW1lbnQpe1xuICAgICAgVC5yaWdodCA9IHRoaXMuaW5zZXJ0KHgsVC5yaWdodClcbiAgICB9XG4gICAgcmV0dXJuIFRcbiAgfVxuICBmaW5kTWluKFQ9VHJlZU5vZGUpe1xuICAgIGlmKFQubGVmdCYmVC5sZWZ0LmVsZW1lbnQhPT0nJyl7XG4gICAgICByZXR1cm4gdGhpcy5maW5kTWluKFQubGVmdClcbiAgICB9ZWxzZSBpZihULmVsZW1lbnQhPT0nJyl7XG4gICAgICByZXR1cm4gVFxuICAgIH1lbHNle1xuICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfVxuICB9XG4gIGZpbmRNYXgoVD1UcmVlTm9kZSl7XG4gICAgaWYoVC5yaWdodCYmVC5yaWdodC5lbGVtZW50IT09Jycpe1xuICAgICAgcmV0dXJuIHRoaXMuZmluZE1heChULnJpZ2h0KVxuICAgIH1lbHNlIGlmKFQuZWxlbWVudCE9PScnKXtcbiAgICAgIHJldHVybiBUXG4gICAgfWVsc2V7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG4gIH1cbiAgRGVsZXRlKHgsVD1UcmVlTm9kZSl7XG4gICAgaWYoVC5lbGVtZW50ID09PSAnJyl7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9ZWxzZSBpZih4PFQuZWxlbWVudCl7XG4gICAgICB0aGlzLkRlbGV0ZSh4LFQubGVmdClcbiAgICB9ZWxzZSBpZih4PlQuZWxlbWVudCl7XG4gICAgICB0aGlzLkRlbGV0ZSh4LFQucmlnaHQpXG4gICAgfWVsc2UgaWYoVC5yaWdodCYmVC5sZWZ0JiZULmxlZnQuZWxlbWVudCE9PScnJiZULnJpZ2h0LmVsZW1lbnQhPT0nJyl7XG4gICAgICBsZXQgbWluVCA9IHRoaXMuZmluZE1pbihULnJpZ2h0KVxuICAgICAgVC5lbGVtZW50ID0gbWluVC5lbGVtZW50XG4gICAgICBULnJpZ2h0ID0gdGhpcy5EZWxldGUoVC5lbGVtZW50LFQucmlnaHQpXG4gICAgfWVsc2V7XG4gICAgICBsZXQgbWluVCA9IFRcbiAgICAgIGlmKFQubGVmdC5lbGVtZW50ID09PSAnJyl7XG4gICAgICAgIFQgPSBULnJpZ2h0XG4gICAgICB9ZWxzZSBpZihULnJpZ2h0LmVsZW1lbnQgPT09ICcnKXtcbiAgICAgICAgVCA9IFQubGVmdFxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gVFxuICB9XG4gIHByaW50KFQ9VHJlZU5vZGUpe1xuICAgIGlmKFQuZWxlbWVudCE9PScnKXtcbiAgICAgIGlmKFQubGVmdC5lbGVtZW50IT09Jycpe1xuICAgICAgICB0aGlzLnByaW50KFQubGVmdClcbiAgICAgIH1cbiAgICAgIGNvbnNvbGUubG9nKFQuZWxlbWVudClcbiAgICAgIGlmKFQucmlnaHQuZWxlbWVudCE9PScnKXtcbiAgICAgICAgdGhpcy5wcmludChULnJpZ2h0KVxuICAgICAgfVxuICAgICAgXG4gICAgfVxuICB9XG4gIG1ha2VFbXB0eSgpe1xuXG4gIH1cbn0iXX0=