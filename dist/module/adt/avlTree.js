'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _binaryTree2 = require('./binaryTree');

var _binaryTree3 = _interopRequireDefault(_binaryTree2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var tree = {
  element: '',
  left: '',
  right: '',
  height: 0
};
var TreeNode = _extends({}, tree);

var avlTree = function (_binaryTree) {
  _inherits(avlTree, _binaryTree);

  function avlTree() {
    _classCallCheck(this, avlTree);

    return _possibleConstructorReturn(this, (avlTree.__proto__ || Object.getPrototypeOf(avlTree)).call(this));
  }

  _createClass(avlTree, [{
    key: 'getTree',
    value: function getTree() {
      return TreeNode;
    }
  }, {
    key: 'insert',
    value: function insert(x) {
      TreeNode = this.insertNode(x, TreeNode);
    }
  }, {
    key: 'insertNode',
    value: function insertNode(x, T) {
      if (T.element === '') {
        T.element = x;
        T.height = 0;
        T.right = _extends({}, tree);
        T.left = _extends({}, tree);
      } else if (x < T.element) {
        T.left = this.insertNode(x, T.left);
        if (T.left.height - T.right.height == 2) {
          if (x < T.left.element) {
            T = this.singleRotateWithLeft(T);
          } else {
            T = this.doubleRotateWithLeft(T);
          }
        }
      } else if (x > T.element) {
        T.right = this.insertNode(x, T.right);
        if (T.right.height - T.left.height == 2) {
          if (x > T.right.element) {
            T = this.singleRotateWithRight(T);
          } else {
            T = this.doubleRotateWithRight(T);
          }
        }
      }
      T.height = Math.max(T.left.height, T.right.height) + 1;
      return T;
    }
  }, {
    key: 'singleRotateWithLeft',
    value: function singleRotateWithLeft(T) {
      var T1 = T.left;
      T.left = T1.right;
      T1.right = T;
      T.height = Math.max(T.left.height, T.right.height) + 1;
      T1.height = Math.max(T1.left.height, T1.right.height) + 1;
      return T1;
    }
  }, {
    key: 'singleRotateWithRight',
    value: function singleRotateWithRight(T) {
      var T1 = T.right;
      T.right = T1.left;
      T1.left = T;
      T.height = Math.max(T.left.height, T.right.height) + 1;
      T1.height = Math.max(T1.left.height, T1.right.height) + 1;
      return T1;
    }
  }, {
    key: 'doubleRotateWithLeft',
    value: function doubleRotateWithLeft(T) {
      T.left = this.singleRotateWithRight(T.left);
      return this.singleRotateWithLeft(T);
    }
  }, {
    key: 'doubleRotateWithRight',
    value: function doubleRotateWithRight(T) {
      T.right = this.singleRotateWithLeft(T.right);
      return this.singleRotateWithRight(T);
    }
  }]);

  return avlTree;
}(_binaryTree3.default);

exports.default = avlTree;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vZHVsZS9hZHQvYXZsVHJlZS5qcyJdLCJuYW1lcyI6WyJ0cmVlIiwiZWxlbWVudCIsImxlZnQiLCJyaWdodCIsImhlaWdodCIsIlRyZWVOb2RlIiwiYXZsVHJlZSIsIngiLCJpbnNlcnROb2RlIiwiVCIsInNpbmdsZVJvdGF0ZVdpdGhMZWZ0IiwiZG91YmxlUm90YXRlV2l0aExlZnQiLCJzaW5nbGVSb3RhdGVXaXRoUmlnaHQiLCJkb3VibGVSb3RhdGVXaXRoUmlnaHQiLCJNYXRoIiwibWF4IiwiVDEiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsT0FBTztBQUNYQyxXQUFRLEVBREc7QUFFWEMsUUFBSyxFQUZNO0FBR1hDLFNBQU0sRUFISztBQUlYQyxVQUFPO0FBSkksQ0FBYjtBQU1BLElBQUlDLHdCQUFlTCxJQUFmLENBQUo7O0lBRXFCTSxPOzs7QUFDbkIscUJBQWE7QUFBQTs7QUFBQTtBQUVaOzs7OzhCQUNRO0FBQ1AsYUFBT0QsUUFBUDtBQUNEOzs7MkJBQ01FLEMsRUFBRTtBQUNQRixpQkFBVyxLQUFLRyxVQUFMLENBQWdCRCxDQUFoQixFQUFrQkYsUUFBbEIsQ0FBWDtBQUNEOzs7K0JBQ1VFLEMsRUFBRUUsQyxFQUFFO0FBQ2IsVUFBR0EsRUFBRVIsT0FBRixLQUFZLEVBQWYsRUFBa0I7QUFDaEJRLFVBQUVSLE9BQUYsR0FBWU0sQ0FBWjtBQUNBRSxVQUFFTCxNQUFGLEdBQVcsQ0FBWDtBQUNBSyxVQUFFTixLQUFGLGdCQUFjSCxJQUFkO0FBQ0FTLFVBQUVQLElBQUYsZ0JBQWFGLElBQWI7QUFDRCxPQUxELE1BS00sSUFBR08sSUFBRUUsRUFBRVIsT0FBUCxFQUFlO0FBQ25CUSxVQUFFUCxJQUFGLEdBQVMsS0FBS00sVUFBTCxDQUFnQkQsQ0FBaEIsRUFBa0JFLEVBQUVQLElBQXBCLENBQVQ7QUFDQSxZQUFHTyxFQUFFUCxJQUFGLENBQU9FLE1BQVAsR0FBZ0JLLEVBQUVOLEtBQUYsQ0FBUUMsTUFBeEIsSUFBa0MsQ0FBckMsRUFBdUM7QUFDckMsY0FBR0csSUFBRUUsRUFBRVAsSUFBRixDQUFPRCxPQUFaLEVBQW9CO0FBQ2xCUSxnQkFBSSxLQUFLQyxvQkFBTCxDQUEwQkQsQ0FBMUIsQ0FBSjtBQUNELFdBRkQsTUFFSztBQUNIQSxnQkFBSSxLQUFLRSxvQkFBTCxDQUEwQkYsQ0FBMUIsQ0FBSjtBQUNEO0FBQ0Y7QUFDRixPQVRLLE1BU0EsSUFBR0YsSUFBRUUsRUFBRVIsT0FBUCxFQUFlO0FBQ25CUSxVQUFFTixLQUFGLEdBQVUsS0FBS0ssVUFBTCxDQUFnQkQsQ0FBaEIsRUFBa0JFLEVBQUVOLEtBQXBCLENBQVY7QUFDQSxZQUFHTSxFQUFFTixLQUFGLENBQVFDLE1BQVIsR0FBaUJLLEVBQUVQLElBQUYsQ0FBT0UsTUFBeEIsSUFBa0MsQ0FBckMsRUFBdUM7QUFDckMsY0FBR0csSUFBRUUsRUFBRU4sS0FBRixDQUFRRixPQUFiLEVBQXFCO0FBQ25CUSxnQkFBSSxLQUFLRyxxQkFBTCxDQUEyQkgsQ0FBM0IsQ0FBSjtBQUNELFdBRkQsTUFFSztBQUNIQSxnQkFBSSxLQUFLSSxxQkFBTCxDQUEyQkosQ0FBM0IsQ0FBSjtBQUNEO0FBQ0Y7QUFDRjtBQUNEQSxRQUFFTCxNQUFGLEdBQVVVLEtBQUtDLEdBQUwsQ0FBU04sRUFBRVAsSUFBRixDQUFPRSxNQUFoQixFQUF1QkssRUFBRU4sS0FBRixDQUFRQyxNQUEvQixJQUF3QyxDQUFsRDtBQUNBLGFBQU9LLENBQVA7QUFDRDs7O3lDQUNvQkEsQyxFQUFFO0FBQ3JCLFVBQUlPLEtBQUtQLEVBQUVQLElBQVg7QUFDQU8sUUFBRVAsSUFBRixHQUFTYyxHQUFHYixLQUFaO0FBQ0FhLFNBQUdiLEtBQUgsR0FBV00sQ0FBWDtBQUNBQSxRQUFFTCxNQUFGLEdBQVdVLEtBQUtDLEdBQUwsQ0FBU04sRUFBRVAsSUFBRixDQUFPRSxNQUFoQixFQUF1QkssRUFBRU4sS0FBRixDQUFRQyxNQUEvQixJQUF5QyxDQUFwRDtBQUNBWSxTQUFHWixNQUFILEdBQVlVLEtBQUtDLEdBQUwsQ0FBU0MsR0FBR2QsSUFBSCxDQUFRRSxNQUFqQixFQUF3QlksR0FBR2IsS0FBSCxDQUFTQyxNQUFqQyxJQUEyQyxDQUF2RDtBQUNBLGFBQU9ZLEVBQVA7QUFDRDs7OzBDQUNxQlAsQyxFQUFFO0FBQ3RCLFVBQUlPLEtBQUtQLEVBQUVOLEtBQVg7QUFDQU0sUUFBRU4sS0FBRixHQUFVYSxHQUFHZCxJQUFiO0FBQ0FjLFNBQUdkLElBQUgsR0FBVU8sQ0FBVjtBQUNBQSxRQUFFTCxNQUFGLEdBQVdVLEtBQUtDLEdBQUwsQ0FBU04sRUFBRVAsSUFBRixDQUFPRSxNQUFoQixFQUF1QkssRUFBRU4sS0FBRixDQUFRQyxNQUEvQixJQUF5QyxDQUFwRDtBQUNBWSxTQUFHWixNQUFILEdBQVlVLEtBQUtDLEdBQUwsQ0FBU0MsR0FBR2QsSUFBSCxDQUFRRSxNQUFqQixFQUF3QlksR0FBR2IsS0FBSCxDQUFTQyxNQUFqQyxJQUEyQyxDQUF2RDtBQUNBLGFBQU9ZLEVBQVA7QUFDRDs7O3lDQUNvQlAsQyxFQUFFO0FBQ3JCQSxRQUFFUCxJQUFGLEdBQVMsS0FBS1UscUJBQUwsQ0FBMkJILEVBQUVQLElBQTdCLENBQVQ7QUFDQSxhQUFPLEtBQUtRLG9CQUFMLENBQTBCRCxDQUExQixDQUFQO0FBQ0Q7OzswQ0FDcUJBLEMsRUFBRTtBQUN0QkEsUUFBRU4sS0FBRixHQUFVLEtBQUtPLG9CQUFMLENBQTBCRCxFQUFFTixLQUE1QixDQUFWO0FBQ0EsYUFBTyxLQUFLUyxxQkFBTCxDQUEyQkgsQ0FBM0IsQ0FBUDtBQUNEOzs7Ozs7a0JBN0RrQkgsTyIsImZpbGUiOiJhdmxUcmVlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGJpbmFyeVRyZWUgZnJvbSAnLi9iaW5hcnlUcmVlJ1xuXG5jb25zdCB0cmVlID0ge1xuICBlbGVtZW50OicnLFxuICBsZWZ0OicnLFxuICByaWdodDonJyxcbiAgaGVpZ2h0OjBcbn1cbmxldCBUcmVlTm9kZSA9IHsuLi50cmVlfVxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBhdmxUcmVlIGV4dGVuZHMgYmluYXJ5VHJlZXtcbiAgY29uc3RydWN0b3IoKXtcbiAgICBzdXBlcigpXG4gIH1cbiAgZ2V0VHJlZSgpe1xuICAgIHJldHVybiBUcmVlTm9kZVxuICB9XG4gIGluc2VydCh4KXtcbiAgICBUcmVlTm9kZSA9IHRoaXMuaW5zZXJ0Tm9kZSh4LFRyZWVOb2RlKVxuICB9XG4gIGluc2VydE5vZGUoeCxUKXtcbiAgICBpZihULmVsZW1lbnQ9PT0nJyl7XG4gICAgICBULmVsZW1lbnQgPSB4XG4gICAgICBULmhlaWdodCA9IDBcbiAgICAgIFQucmlnaHQgPSB7Li4udHJlZX1cbiAgICAgIFQubGVmdCA9IHsuLi50cmVlfVxuICAgIH1lbHNlIGlmKHg8VC5lbGVtZW50KXtcbiAgICAgIFQubGVmdCA9IHRoaXMuaW5zZXJ0Tm9kZSh4LFQubGVmdClcbiAgICAgIGlmKFQubGVmdC5oZWlnaHQgLSBULnJpZ2h0LmhlaWdodCA9PSAyKXtcbiAgICAgICAgaWYoeDxULmxlZnQuZWxlbWVudCl7XG4gICAgICAgICAgVCA9IHRoaXMuc2luZ2xlUm90YXRlV2l0aExlZnQoVClcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgVCA9IHRoaXMuZG91YmxlUm90YXRlV2l0aExlZnQoVClcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1lbHNlIGlmKHg+VC5lbGVtZW50KXtcbiAgICAgIFQucmlnaHQgPSB0aGlzLmluc2VydE5vZGUoeCxULnJpZ2h0KVxuICAgICAgaWYoVC5yaWdodC5oZWlnaHQgLSBULmxlZnQuaGVpZ2h0ID09IDIpe1xuICAgICAgICBpZih4PlQucmlnaHQuZWxlbWVudCl7XG4gICAgICAgICAgVCA9IHRoaXMuc2luZ2xlUm90YXRlV2l0aFJpZ2h0KFQpXG4gICAgICAgIH1lbHNle1xuICAgICAgICAgIFQgPSB0aGlzLmRvdWJsZVJvdGF0ZVdpdGhSaWdodChUKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIFQuaGVpZ2h0ID1NYXRoLm1heChULmxlZnQuaGVpZ2h0LFQucmlnaHQuaGVpZ2h0KSArMVxuICAgIHJldHVybiBUXG4gIH1cbiAgc2luZ2xlUm90YXRlV2l0aExlZnQoVCl7XG4gICAgbGV0IFQxID0gVC5sZWZ0XG4gICAgVC5sZWZ0ID0gVDEucmlnaHRcbiAgICBUMS5yaWdodCA9IFRcbiAgICBULmhlaWdodCA9IE1hdGgubWF4KFQubGVmdC5oZWlnaHQsVC5yaWdodC5oZWlnaHQpICsgMVxuICAgIFQxLmhlaWdodCA9IE1hdGgubWF4KFQxLmxlZnQuaGVpZ2h0LFQxLnJpZ2h0LmhlaWdodCkgKyAxXG4gICAgcmV0dXJuIFQxXG4gIH1cbiAgc2luZ2xlUm90YXRlV2l0aFJpZ2h0KFQpe1xuICAgIGxldCBUMSA9IFQucmlnaHRcbiAgICBULnJpZ2h0ID0gVDEubGVmdFxuICAgIFQxLmxlZnQgPSBUXG4gICAgVC5oZWlnaHQgPSBNYXRoLm1heChULmxlZnQuaGVpZ2h0LFQucmlnaHQuaGVpZ2h0KSArIDFcbiAgICBUMS5oZWlnaHQgPSBNYXRoLm1heChUMS5sZWZ0LmhlaWdodCxUMS5yaWdodC5oZWlnaHQpICsgMVxuICAgIHJldHVybiBUMVxuICB9XG4gIGRvdWJsZVJvdGF0ZVdpdGhMZWZ0KFQpe1xuICAgIFQubGVmdCA9IHRoaXMuc2luZ2xlUm90YXRlV2l0aFJpZ2h0KFQubGVmdClcbiAgICByZXR1cm4gdGhpcy5zaW5nbGVSb3RhdGVXaXRoTGVmdChUKVxuICB9XG4gIGRvdWJsZVJvdGF0ZVdpdGhSaWdodChUKXtcbiAgICBULnJpZ2h0ID0gdGhpcy5zaW5nbGVSb3RhdGVXaXRoTGVmdChULnJpZ2h0KVxuICAgIHJldHVybiB0aGlzLnNpbmdsZVJvdGF0ZVdpdGhSaWdodChUKVxuICB9XG59Il19