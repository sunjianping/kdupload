'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chainTable = require('./chainTable');

var _chainTable2 = _interopRequireDefault(_chainTable);

var _nextPrime = require('../runtime/nextPrime');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var hashTree = function () {
  function hashTree(size) {
    _classCallCheck(this, hashTree);

    this.tableSize = (0, _nextPrime.nextPrime)(size);
    console.log(this.tableSize);
    this.table = new Array(this.tableSize).fill(0);
  }

  _createClass(hashTree, [{
    key: 'hash',
    value: function hash(x) {
      var key = x + '',
          i = 0,
          len = key.length,
          hashVal = 0;
      while (len !== i) {
        hashVal = (hashVal << 5) + key[i++];
      }
      return hashVal % this.tableSize;
    }
  }, {
    key: 'insert',
    value: function insert(x) {
      var hash = this.hash(x);
      var ctable = this.table[hash];
      if (!ctable) {
        ctable = new _chainTable2.default();
        this.table[hash] = ctable;
      }

      ctable.insert(x);
    }
  }, {
    key: 'find',
    value: function find(x) {
      var ctable = this.table[this.hash(x)];
      return ctable.find(x);
    }
  }]);

  return hashTree;
}();

exports.default = hashTree;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vZHVsZS9hZHQvaGFzaFRhYmxlLmpzIl0sIm5hbWVzIjpbImhhc2hUcmVlIiwic2l6ZSIsInRhYmxlU2l6ZSIsImNvbnNvbGUiLCJsb2ciLCJ0YWJsZSIsIkFycmF5IiwiZmlsbCIsIngiLCJrZXkiLCJpIiwibGVuIiwibGVuZ3RoIiwiaGFzaFZhbCIsImhhc2giLCJjdGFibGUiLCJpbnNlcnQiLCJmaW5kIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBOzs7O0FBQ0E7Ozs7OztJQUNxQkEsUTtBQUNuQixvQkFBWUMsSUFBWixFQUFpQjtBQUFBOztBQUNmLFNBQUtDLFNBQUwsR0FBaUIsMEJBQVVELElBQVYsQ0FBakI7QUFDQUUsWUFBUUMsR0FBUixDQUFZLEtBQUtGLFNBQWpCO0FBQ0EsU0FBS0csS0FBTCxHQUFhLElBQUlDLEtBQUosQ0FBVSxLQUFLSixTQUFmLEVBQTBCSyxJQUExQixDQUErQixDQUEvQixDQUFiO0FBQ0Q7Ozs7eUJBQ0lDLEMsRUFBRTtBQUNMLFVBQUlDLE1BQU1ELElBQUUsRUFBWjtBQUFBLFVBQWVFLElBQUksQ0FBbkI7QUFBQSxVQUFxQkMsTUFBTUYsSUFBSUcsTUFBL0I7QUFBQSxVQUFzQ0MsVUFBVSxDQUFoRDtBQUNBLGFBQU1GLFFBQU1ELENBQVosRUFBYztBQUNaRyxrQkFBVSxDQUFDQSxXQUFTLENBQVYsSUFBZUosSUFBSUMsR0FBSixDQUF6QjtBQUNEO0FBQ0QsYUFBT0csVUFBVSxLQUFLWCxTQUF0QjtBQUNEOzs7MkJBQ01NLEMsRUFBRTtBQUNQLFVBQUlNLE9BQU8sS0FBS0EsSUFBTCxDQUFVTixDQUFWLENBQVg7QUFDQSxVQUFJTyxTQUFTLEtBQUtWLEtBQUwsQ0FBV1MsSUFBWCxDQUFiO0FBQ0EsVUFBRyxDQUFDQyxNQUFKLEVBQVc7QUFDVEEsaUJBQVMsMEJBQVQ7QUFDQSxhQUFLVixLQUFMLENBQVdTLElBQVgsSUFBbUJDLE1BQW5CO0FBQ0Q7O0FBRURBLGFBQU9DLE1BQVAsQ0FBY1IsQ0FBZDtBQUNEOzs7eUJBQ0lBLEMsRUFBRTtBQUNMLFVBQUlPLFNBQVMsS0FBS1YsS0FBTCxDQUFXLEtBQUtTLElBQUwsQ0FBVU4sQ0FBVixDQUFYLENBQWI7QUFDQSxhQUFPTyxPQUFPRSxJQUFQLENBQVlULENBQVosQ0FBUDtBQUNEOzs7Ozs7a0JBMUJrQlIsUSIsImZpbGUiOiJoYXNoVGFibGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgY2hhaW5UYWJsZSBmcm9tICcuL2NoYWluVGFibGUnXG5pbXBvcnQge25leHRQcmltZX0gZnJvbSAnLi4vcnVudGltZS9uZXh0UHJpbWUnXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBoYXNoVHJlZXtcbiAgY29uc3RydWN0b3Ioc2l6ZSl7XG4gICAgdGhpcy50YWJsZVNpemUgPSBuZXh0UHJpbWUoc2l6ZSlcbiAgICBjb25zb2xlLmxvZyh0aGlzLnRhYmxlU2l6ZSlcbiAgICB0aGlzLnRhYmxlID0gbmV3IEFycmF5KHRoaXMudGFibGVTaXplKS5maWxsKDApIFxuICB9XG4gIGhhc2goeCl7XG4gICAgbGV0IGtleSA9IHgrJycsaSA9IDAsbGVuID0ga2V5Lmxlbmd0aCxoYXNoVmFsID0gMFxuICAgIHdoaWxlKGxlbiE9PWkpe1xuICAgICAgaGFzaFZhbCA9IChoYXNoVmFsPDw1KSArIGtleVtpKytdXG4gICAgfVxuICAgIHJldHVybiBoYXNoVmFsICUgdGhpcy50YWJsZVNpemVcbiAgfVxuICBpbnNlcnQoeCl7XG4gICAgbGV0IGhhc2ggPSB0aGlzLmhhc2goeClcbiAgICBsZXQgY3RhYmxlID0gdGhpcy50YWJsZVtoYXNoXVxuICAgIGlmKCFjdGFibGUpe1xuICAgICAgY3RhYmxlID0gbmV3IGNoYWluVGFibGUoKVxuICAgICAgdGhpcy50YWJsZVtoYXNoXSA9IGN0YWJsZVxuICAgIH1cbiAgICBcbiAgICBjdGFibGUuaW5zZXJ0KHgpXG4gIH1cbiAgZmluZCh4KXtcbiAgICBsZXQgY3RhYmxlID0gdGhpcy50YWJsZVt0aGlzLmhhc2goeCldXG4gICAgcmV0dXJuIGN0YWJsZS5maW5kKHgpXG4gIH1cbn0iXX0=