'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.md5EventType = exports.EventProcessTimes = undefined;

var _enum = require('enum');

var _enum2 = _interopRequireDefault(_enum);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EventProcessTimes = exports.EventProcessTimes = new _enum2.default({
    ONCE: 'ONCE',
    INFINITY: 'INFINITY'
});

var md5EventType = exports.md5EventType = {};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vZHVsZS9jb25zdGFudHMvZXZlbnQuanMiXSwibmFtZXMiOlsiRXZlbnRQcm9jZXNzVGltZXMiLCJPTkNFIiwiSU5GSU5JVFkiLCJtZDVFdmVudFR5cGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7Ozs7O0FBR08sSUFBTUEsZ0RBQW9CLG1CQUFTO0FBQ3RDQyxVQUFLLE1BRGlDO0FBRXRDQyxjQUFTO0FBRjZCLENBQVQsQ0FBMUI7O0FBS0EsSUFBTUMsc0NBQWUsRUFBckIiLCJmaWxlIjoiZXZlbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRW51bSBmcm9tICdlbnVtJ1xuXG5cbmV4cG9ydCBjb25zdCBFdmVudFByb2Nlc3NUaW1lcyA9IG5ldyBFbnVtKHtcbiAgICBPTkNFOidPTkNFJyxcbiAgICBJTkZJTklUWTonSU5GSU5JVFknLFxufSlcblxuZXhwb3J0IGNvbnN0IG1kNUV2ZW50VHlwZSA9IHtcbiAgXG59Il19