"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Queue = function () {
  function Queue() {
    _classCallCheck(this, Queue);

    this.items = [];
  }

  _createClass(Queue, [{
    key: "enqueue",
    value: function enqueue(element) {
      this.items.push(element);
    }
  }, {
    key: "dequeue",
    value: function dequeue() {
      return this.items.shift();
    }
  }, {
    key: "front",
    value: function front() {
      return this.items[0];
    }
  }, {
    key: "isEmpty",
    value: function isEmpty() {
      return this.items.length == 0;
    }
  }, {
    key: "size",
    value: function size() {
      return this.items.length;
    }
  }, {
    key: "clear",
    value: function clear() {
      this.items = [];
    }
  }, {
    key: "each",
    value: function each(cb) {
      this.items.forEach(cb);
    }
  }, {
    key: "clearToFlag",
    value: function clearToFlag(flag) {
      var len = this.items.length,
          i = 0;
      for (; i < len; i++) {
        var item = this.items[i];
        if (item[flag] === true) {
          this.items.splice(i, 1);
          i--;
          len--;
        }
      }
    }
  }]);

  return Queue;
}();

exports.default = Queue;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vZHVsZS9ydW50aW1lL3F1ZXVlLmpzIl0sIm5hbWVzIjpbIlF1ZXVlIiwiaXRlbXMiLCJlbGVtZW50IiwicHVzaCIsInNoaWZ0IiwibGVuZ3RoIiwiY2IiLCJmb3JFYWNoIiwiZmxhZyIsImxlbiIsImkiLCJpdGVtIiwic3BsaWNlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0lBQXFCQSxLO0FBQ25CLG1CQUFhO0FBQUE7O0FBQ1gsU0FBS0MsS0FBTCxHQUFhLEVBQWI7QUFDRDs7Ozs0QkFDT0MsTyxFQUFRO0FBQ2QsV0FBS0QsS0FBTCxDQUFXRSxJQUFYLENBQWdCRCxPQUFoQjtBQUNEOzs7OEJBQ1E7QUFDUCxhQUFPLEtBQUtELEtBQUwsQ0FBV0csS0FBWCxFQUFQO0FBQ0Q7Ozs0QkFDTTtBQUNMLGFBQU8sS0FBS0gsS0FBTCxDQUFXLENBQVgsQ0FBUDtBQUNEOzs7OEJBQ1E7QUFDUCxhQUFPLEtBQUtBLEtBQUwsQ0FBV0ksTUFBWCxJQUFxQixDQUE1QjtBQUNEOzs7MkJBQ0s7QUFDSixhQUFPLEtBQUtKLEtBQUwsQ0FBV0ksTUFBbEI7QUFDRDs7OzRCQUNNO0FBQ0wsV0FBS0osS0FBTCxHQUFhLEVBQWI7QUFDRDs7O3lCQUNJSyxFLEVBQUc7QUFDTixXQUFLTCxLQUFMLENBQVdNLE9BQVgsQ0FBbUJELEVBQW5CO0FBQ0Q7OztnQ0FDV0UsSSxFQUFLO0FBQ2YsVUFBSUMsTUFBTSxLQUFLUixLQUFMLENBQVdJLE1BQXJCO0FBQUEsVUFBOEJLLElBQUksQ0FBbEM7QUFDQSxhQUFLQSxJQUFFRCxHQUFQLEVBQVdDLEdBQVgsRUFBZTtBQUNiLFlBQUlDLE9BQU8sS0FBS1YsS0FBTCxDQUFXUyxDQUFYLENBQVg7QUFDQSxZQUFHQyxLQUFLSCxJQUFMLE1BQWUsSUFBbEIsRUFBdUI7QUFDckIsZUFBS1AsS0FBTCxDQUFXVyxNQUFYLENBQWtCRixDQUFsQixFQUFvQixDQUFwQjtBQUNBQTtBQUNBRDtBQUNEO0FBQ0Y7QUFDRjs7Ozs7O2tCQW5Da0JULEsiLCJmaWxlIjoicXVldWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBjbGFzcyBRdWV1ZSB7XG4gIGNvbnN0cnVjdG9yKCl7XG4gICAgdGhpcy5pdGVtcyA9IFtdXG4gIH1cbiAgZW5xdWV1ZShlbGVtZW50KXtcbiAgICB0aGlzLml0ZW1zLnB1c2goZWxlbWVudClcbiAgfVxuICBkZXF1ZXVlKCl7XG4gICAgcmV0dXJuIHRoaXMuaXRlbXMuc2hpZnQoKVxuICB9XG4gIGZyb250KCl7XG4gICAgcmV0dXJuIHRoaXMuaXRlbXNbMF1cbiAgfVxuICBpc0VtcHR5KCl7XG4gICAgcmV0dXJuIHRoaXMuaXRlbXMubGVuZ3RoID09IDBcbiAgfVxuICBzaXplKCl7XG4gICAgcmV0dXJuIHRoaXMuaXRlbXMubGVuZ3RoXG4gIH1cbiAgY2xlYXIoKXtcbiAgICB0aGlzLml0ZW1zID0gW11cbiAgfVxuICBlYWNoKGNiKXtcbiAgICB0aGlzLml0ZW1zLmZvckVhY2goY2IpICAgIFxuICB9XG4gIGNsZWFyVG9GbGFnKGZsYWcpe1xuICAgIGxldCBsZW4gPSB0aGlzLml0ZW1zLmxlbmd0aCAsIGkgPSAwXG4gICAgZm9yKDtpPGxlbjtpKyspe1xuICAgICAgbGV0IGl0ZW0gPSB0aGlzLml0ZW1zW2ldXG4gICAgICBpZihpdGVtW2ZsYWddID09PSB0cnVlKXtcbiAgICAgICAgdGhpcy5pdGVtcy5zcGxpY2UoaSwxKVxuICAgICAgICBpLS1cbiAgICAgICAgbGVuLS1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn0iXX0=