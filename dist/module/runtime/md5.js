'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _sparkMd = require('spark-md5');

var _sparkMd2 = _interopRequireDefault(_sparkMd);

var _chunk = require('../constants/chunk');

var _event = require('./event');

var _event2 = _interopRequireDefault(_event);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var fileMd5 = function (_EventEmitter) {
    _inherits(fileMd5, _EventEmitter);

    function fileMd5(file, options) {
        _classCallCheck(this, fileMd5);

        var _this = _possibleConstructorReturn(this, (fileMd5.__proto__ || Object.getPrototypeOf(fileMd5)).call(this));

        _this.file = file;
        _this.chunkSize = options.chunkSize || _chunk.CHUNK.SIZE; // Read in chunks of 2MB
        _this.chunks = Math.ceil(file.size / _this.chunkSize);
        _this.currentChunk = 0;
        _this.spark = new _sparkMd2.default.ArrayBuffer();
        _this.fileReader = new FileReader();
        fileReader.onload = _this.onload;
        fileReader.onerror = _this.onerror;
        fileReader.onprogress = _this.onProcess;
        _this.loadNext();
        return _this;
    }

    _createClass(fileMd5, [{
        key: 'blobSlice',
        value: function blobSlice(file) {
            var slice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice;

            for (var _len = arguments.length, param = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                param[_key - 1] = arguments[_key];
            }

            return slice.apply(file, param);
        }
    }, {
        key: 'loadNext',
        value: function loadNext() {
            var start = this.currentChunk * this.chunkSize,
                end = start + this.chunkSize >= this.file.size ? this.file.size : start + this.chunkSize;

            this.fileReader.readAsArrayBuffer(this.blobSlice(this.file, start, end));
        }
    }, {
        key: 'onerror',
        value: function onerror() {
            console.warn('oops, something went wrong.');
        }
    }, {
        key: 'onload',
        value: function onload(e) {
            console.log('read chunk nr', this.currentChunk + 1, 'of', this.chunks);
            this.spark.append(e.target.result); // Append array buffer
            this.currentChunk++;

            if (this.currentChunk < this.chunks) {
                this.loadNext();
            } else {
                console.log('finished loading');
                console.info('computed hash', this.spark.end()); // Compute hash
            }
        }
    }, {
        key: 'onProcess',
        value: function onProcess() {
            console.log(arguments);
        }
    }, {
        key: 'onEnd',
        value: function onEnd() {}
    }]);

    return fileMd5;
}(_event2.default);

exports.default = fileMd5;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vZHVsZS9ydW50aW1lL21kNS5qcyJdLCJuYW1lcyI6WyJmaWxlTWQ1IiwiZmlsZSIsIm9wdGlvbnMiLCJjaHVua1NpemUiLCJTSVpFIiwiY2h1bmtzIiwiTWF0aCIsImNlaWwiLCJzaXplIiwiY3VycmVudENodW5rIiwic3BhcmsiLCJBcnJheUJ1ZmZlciIsImZpbGVSZWFkZXIiLCJGaWxlUmVhZGVyIiwib25sb2FkIiwib25lcnJvciIsIm9ucHJvZ3Jlc3MiLCJvblByb2Nlc3MiLCJsb2FkTmV4dCIsInNsaWNlIiwiRmlsZSIsInByb3RvdHlwZSIsIm1velNsaWNlIiwid2Via2l0U2xpY2UiLCJwYXJhbSIsImFwcGx5Iiwic3RhcnQiLCJlbmQiLCJyZWFkQXNBcnJheUJ1ZmZlciIsImJsb2JTbGljZSIsImNvbnNvbGUiLCJ3YXJuIiwiZSIsImxvZyIsImFwcGVuZCIsInRhcmdldCIsInJlc3VsdCIsImluZm8iLCJhcmd1bWVudHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7Ozs7QUFDQTs7QUFDQTs7Ozs7Ozs7Ozs7O0lBRXFCQSxPOzs7QUFDakIscUJBQVlDLElBQVosRUFBaUJDLE9BQWpCLEVBQXlCO0FBQUE7O0FBQUE7O0FBRXJCLGNBQUtELElBQUwsR0FBWUEsSUFBWjtBQUNBLGNBQUtFLFNBQUwsR0FBaUJELFFBQVFDLFNBQVIsSUFBcUIsYUFBTUMsSUFBNUMsQ0FIcUIsQ0FHd0Q7QUFDN0UsY0FBS0MsTUFBTCxHQUFjQyxLQUFLQyxJQUFMLENBQVVOLEtBQUtPLElBQUwsR0FBWSxNQUFLTCxTQUEzQixDQUFkO0FBQ0EsY0FBS00sWUFBTCxHQUFvQixDQUFwQjtBQUNBLGNBQUtDLEtBQUwsR0FBYSxJQUFJLGtCQUFTQyxXQUFiLEVBQWI7QUFDQSxjQUFLQyxVQUFMLEdBQWtCLElBQUlDLFVBQUosRUFBbEI7QUFDQUQsbUJBQVdFLE1BQVgsR0FBb0IsTUFBS0EsTUFBekI7QUFDQUYsbUJBQVdHLE9BQVgsR0FBcUIsTUFBS0EsT0FBMUI7QUFDQUgsbUJBQVdJLFVBQVgsR0FBd0IsTUFBS0MsU0FBN0I7QUFDQSxjQUFLQyxRQUFMO0FBWHFCO0FBWXhCOzs7O2tDQUVTakIsSSxFQUFjO0FBQ3BCLGdCQUFJa0IsUUFBUUMsS0FBS0MsU0FBTCxDQUFlRixLQUFmLElBQXdCQyxLQUFLQyxTQUFMLENBQWVDLFFBQXZDLElBQW1ERixLQUFLQyxTQUFMLENBQWVFLFdBQTlFOztBQURvQiw4Q0FBTkMsS0FBTTtBQUFOQSxxQkFBTTtBQUFBOztBQUVwQixtQkFBT0wsTUFBTU0sS0FBTixDQUFZeEIsSUFBWixFQUFpQnVCLEtBQWpCLENBQVA7QUFDSDs7O21DQUNTO0FBQ04sZ0JBQUlFLFFBQVEsS0FBS2pCLFlBQUwsR0FBb0IsS0FBS04sU0FBckM7QUFBQSxnQkFDUXdCLE1BQVFELFFBQVEsS0FBS3ZCLFNBQWQsSUFBNEIsS0FBS0YsSUFBTCxDQUFVTyxJQUF2QyxHQUErQyxLQUFLUCxJQUFMLENBQVVPLElBQXpELEdBQWdFa0IsUUFBUSxLQUFLdkIsU0FEM0Y7O0FBR0EsaUJBQUtTLFVBQUwsQ0FBZ0JnQixpQkFBaEIsQ0FBa0MsS0FBS0MsU0FBTCxDQUFlLEtBQUs1QixJQUFwQixFQUEwQnlCLEtBQTFCLEVBQWlDQyxHQUFqQyxDQUFsQztBQUNIOzs7a0NBQ1E7QUFDTEcsb0JBQVFDLElBQVIsQ0FBYSw2QkFBYjtBQUNIOzs7K0JBQ01DLEMsRUFBRTtBQUNMRixvQkFBUUcsR0FBUixDQUFZLGVBQVosRUFBNkIsS0FBS3hCLFlBQUwsR0FBb0IsQ0FBakQsRUFBb0QsSUFBcEQsRUFBMEQsS0FBS0osTUFBL0Q7QUFDQSxpQkFBS0ssS0FBTCxDQUFXd0IsTUFBWCxDQUFrQkYsRUFBRUcsTUFBRixDQUFTQyxNQUEzQixFQUZLLENBRWdEO0FBQ3JELGlCQUFLM0IsWUFBTDs7QUFFQSxnQkFBSSxLQUFLQSxZQUFMLEdBQW9CLEtBQUtKLE1BQTdCLEVBQXFDO0FBQ2pDLHFCQUFLYSxRQUFMO0FBQ0gsYUFGRCxNQUVPO0FBQ0hZLHdCQUFRRyxHQUFSLENBQVksa0JBQVo7QUFDQUgsd0JBQVFPLElBQVIsQ0FBYSxlQUFiLEVBQThCLEtBQUszQixLQUFMLENBQVdpQixHQUFYLEVBQTlCLEVBRkcsQ0FFOEM7QUFDcEQ7QUFDSjs7O29DQUNVO0FBQ1BHLG9CQUFRRyxHQUFSLENBQVlLLFNBQVo7QUFDSDs7O2dDQUNNLENBRU47Ozs7OztrQkE3Q2dCdEMsTyIsImZpbGUiOiJtZDUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU3BhcmtNRDUgZnJvbSAnc3BhcmstbWQ1J1xuaW1wb3J0IHsgQ0hVTksgfSBmcm9tICcuLi9jb25zdGFudHMvY2h1bmsnXG5pbXBvcnQgRXZlbnRFbWl0dGVyIGZyb20gJy4vZXZlbnQnXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGZpbGVNZDUgZXh0ZW5kcyBFdmVudEVtaXR0ZXJ7XG4gICAgY29uc3RydWN0b3IoZmlsZSxvcHRpb25zKXtcbiAgICAgICAgc3VwZXIoKVxuICAgICAgICB0aGlzLmZpbGUgPSBmaWxlXG4gICAgICAgIHRoaXMuY2h1bmtTaXplID0gb3B0aW9ucy5jaHVua1NpemUgfHwgQ0hVTksuU0laRSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gUmVhZCBpbiBjaHVua3Mgb2YgMk1CXG4gICAgICAgIHRoaXMuY2h1bmtzID0gTWF0aC5jZWlsKGZpbGUuc2l6ZSAvIHRoaXMuY2h1bmtTaXplKVxuICAgICAgICB0aGlzLmN1cnJlbnRDaHVuayA9IDBcbiAgICAgICAgdGhpcy5zcGFyayA9IG5ldyBTcGFya01ENS5BcnJheUJ1ZmZlcigpXG4gICAgICAgIHRoaXMuZmlsZVJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKClcbiAgICAgICAgZmlsZVJlYWRlci5vbmxvYWQgPSB0aGlzLm9ubG9hZFxuICAgICAgICBmaWxlUmVhZGVyLm9uZXJyb3IgPSB0aGlzLm9uZXJyb3JcbiAgICAgICAgZmlsZVJlYWRlci5vbnByb2dyZXNzID0gdGhpcy5vblByb2Nlc3NcbiAgICAgICAgdGhpcy5sb2FkTmV4dCgpXG4gICAgfVxuXG4gICAgYmxvYlNsaWNlKGZpbGUsLi4ucGFyYW0pe1xuICAgICAgICBsZXQgc2xpY2UgPSBGaWxlLnByb3RvdHlwZS5zbGljZSB8fCBGaWxlLnByb3RvdHlwZS5tb3pTbGljZSB8fCBGaWxlLnByb3RvdHlwZS53ZWJraXRTbGljZVxuICAgICAgICByZXR1cm4gc2xpY2UuYXBwbHkoZmlsZSxwYXJhbSlcbiAgICB9XG4gICAgbG9hZE5leHQoKXtcbiAgICAgICAgdmFyIHN0YXJ0ID0gdGhpcy5jdXJyZW50Q2h1bmsgKiB0aGlzLmNodW5rU2l6ZSxcbiAgICAgICAgICAgICAgICBlbmQgPSAoKHN0YXJ0ICsgdGhpcy5jaHVua1NpemUpID49IHRoaXMuZmlsZS5zaXplKSA/IHRoaXMuZmlsZS5zaXplIDogc3RhcnQgKyB0aGlzLmNodW5rU2l6ZVxuXG4gICAgICAgIHRoaXMuZmlsZVJlYWRlci5yZWFkQXNBcnJheUJ1ZmZlcih0aGlzLmJsb2JTbGljZSh0aGlzLmZpbGUsIHN0YXJ0LCBlbmQpKVxuICAgIH1cbiAgICBvbmVycm9yKCl7XG4gICAgICAgIGNvbnNvbGUud2Fybignb29wcywgc29tZXRoaW5nIHdlbnQgd3JvbmcuJylcbiAgICB9XG4gICAgb25sb2FkKGUpe1xuICAgICAgICBjb25zb2xlLmxvZygncmVhZCBjaHVuayBucicsIHRoaXMuY3VycmVudENodW5rICsgMSwgJ29mJywgdGhpcy5jaHVua3MpXG4gICAgICAgIHRoaXMuc3BhcmsuYXBwZW5kKGUudGFyZ2V0LnJlc3VsdCkgICAgICAgICAgICAgICAgICAgLy8gQXBwZW5kIGFycmF5IGJ1ZmZlclxuICAgICAgICB0aGlzLmN1cnJlbnRDaHVuaysrXG5cbiAgICAgICAgaWYgKHRoaXMuY3VycmVudENodW5rIDwgdGhpcy5jaHVua3MpIHtcbiAgICAgICAgICAgIHRoaXMubG9hZE5leHQoKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2ZpbmlzaGVkIGxvYWRpbmcnKVxuICAgICAgICAgICAgY29uc29sZS5pbmZvKCdjb21wdXRlZCBoYXNoJywgdGhpcy5zcGFyay5lbmQoKSkgIC8vIENvbXB1dGUgaGFzaFxuICAgICAgICB9XG4gICAgfVxuICAgIG9uUHJvY2Vzcygpe1xuICAgICAgICBjb25zb2xlLmxvZyhhcmd1bWVudHMpXG4gICAgfVxuICAgIG9uRW5kKCl7XG5cbiAgICB9XG59Il19