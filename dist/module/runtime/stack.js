"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stack = function () {
  function Stack() {
    _classCallCheck(this, Stack);

    this.items = [];
  }

  _createClass(Stack, [{
    key: "push",
    value: function push(element) {
      this.items.push(element);
    }
  }, {
    key: "pop",
    value: function pop() {
      return this.items.pop();
    }
  }, {
    key: "peek",
    value: function peek() {
      return this.items[this.items.length - 1];
    }
  }, {
    key: "isEmpty",
    value: function isEmpty() {
      return this.items.length == 0;
    }
  }, {
    key: "clear",
    value: function clear() {
      this.items = [];
    }
  }, {
    key: "size",
    value: function size() {
      return this.items.length;
    }
  }, {
    key: "print",
    value: function print() {
      console.log(this.items.toString());
    }
  }]);

  return Stack;
}();

exports.default = Stack;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21vZHVsZS9ydW50aW1lL3N0YWNrLmpzIl0sIm5hbWVzIjpbIlN0YWNrIiwiaXRlbXMiLCJlbGVtZW50IiwicHVzaCIsInBvcCIsImxlbmd0aCIsImNvbnNvbGUiLCJsb2ciLCJ0b1N0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztJQUFxQkEsSztBQUNuQixtQkFBYTtBQUFBOztBQUNYLFNBQUtDLEtBQUwsR0FBYSxFQUFiO0FBQ0Q7Ozs7eUJBQ0lDLE8sRUFBUTtBQUNYLFdBQUtELEtBQUwsQ0FBV0UsSUFBWCxDQUFnQkQsT0FBaEI7QUFDRDs7OzBCQUNJO0FBQ0gsYUFBTyxLQUFLRCxLQUFMLENBQVdHLEdBQVgsRUFBUDtBQUNEOzs7MkJBQ0s7QUFDSixhQUFPLEtBQUtILEtBQUwsQ0FBVyxLQUFLQSxLQUFMLENBQVdJLE1BQVgsR0FBb0IsQ0FBL0IsQ0FBUDtBQUNEOzs7OEJBQ1E7QUFDUCxhQUFPLEtBQUtKLEtBQUwsQ0FBV0ksTUFBWCxJQUFxQixDQUE1QjtBQUNEOzs7NEJBQ007QUFDTCxXQUFLSixLQUFMLEdBQWEsRUFBYjtBQUNEOzs7MkJBQ0s7QUFDSixhQUFPLEtBQUtBLEtBQUwsQ0FBV0ksTUFBbEI7QUFDRDs7OzRCQUNNO0FBQ0xDLGNBQVFDLEdBQVIsQ0FBWSxLQUFLTixLQUFMLENBQVdPLFFBQVgsRUFBWjtBQUNEOzs7Ozs7a0JBeEJrQlIsSyIsImZpbGUiOiJzdGFjay5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0YWNre1xuICBjb25zdHJ1Y3Rvcigpe1xuICAgIHRoaXMuaXRlbXMgPSBbXVxuICB9XG4gIHB1c2goZWxlbWVudCl7XG4gICAgdGhpcy5pdGVtcy5wdXNoKGVsZW1lbnQpXG4gIH1cbiAgcG9wKCl7XG4gICAgcmV0dXJuIHRoaXMuaXRlbXMucG9wKClcbiAgfVxuICBwZWVrKCl7XG4gICAgcmV0dXJuIHRoaXMuaXRlbXNbdGhpcy5pdGVtcy5sZW5ndGggLSAxXVxuICB9XG4gIGlzRW1wdHkoKXtcbiAgICByZXR1cm4gdGhpcy5pdGVtcy5sZW5ndGggPT0gMFxuICB9XG4gIGNsZWFyKCl7XG4gICAgdGhpcy5pdGVtcyA9IFtdXG4gIH1cbiAgc2l6ZSgpe1xuICAgIHJldHVybiB0aGlzLml0ZW1zLmxlbmd0aFxuICB9XG4gIHByaW50KCl7XG4gICAgY29uc29sZS5sb2codGhpcy5pdGVtcy50b1N0cmluZygpKVxuICB9XG59Il19