'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _md = require('./runtime/md5');

var _md2 = _interopRequireDefault(_md);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var KdPanUploader = function () {
    function KdPanUploader() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, KdPanUploader);

        this.options = options;
    }

    _createClass(KdPanUploader, [{
        key: 'kd_md5',
        value: function kd_md5(string) {
            var md5obj = md5(string);
            return md5obj;
        }
    }]);

    return KdPanUploader;
}();

exports.default = KdPanUploader;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL21vZHVsZS9rZFBhblVwbG9hZGVyLmpzIl0sIm5hbWVzIjpbIktkUGFuVXBsb2FkZXIiLCJvcHRpb25zIiwic3RyaW5nIiwibWQ1b2JqIiwibWQ1Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBOzs7Ozs7OztJQUVxQkEsYTtBQUVqQiw2QkFBMEI7QUFBQSxZQUFiQyxPQUFhLHVFQUFILEVBQUc7O0FBQUE7O0FBQ3ZCLGFBQUtBLE9BQUwsR0FBZUEsT0FBZjtBQUNGOzs7OytCQUNNQyxNLEVBQU87QUFDVixnQkFBTUMsU0FBU0MsSUFBSUYsTUFBSixDQUFmO0FBQ0EsbUJBQU9DLE1BQVA7QUFDSDs7Ozs7O2tCQVJnQkgsYSIsImZpbGUiOiJrZFBhblVwbG9hZGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGZpbGVNZDUgZnJvbSAnLi9ydW50aW1lL21kNSdcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgS2RQYW5VcGxvYWRlciB7XG5cbiAgICBjb25zdHJ1Y3RvciAob3B0aW9ucyA9IHt9KXtcbiAgICAgICB0aGlzLm9wdGlvbnMgPSBvcHRpb25zXG4gICAgfVxuICAgIGtkX21kNShzdHJpbmcpe1xuICAgICAgICBjb25zdCBtZDVvYmogPSBtZDUoc3RyaW5nKVxuICAgICAgICByZXR1cm4gbWQ1b2JqXG4gICAgfVxufSJdfQ==