'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _queue = require('./queue');

var _queue2 = _interopRequireDefault(_queue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PriorityQueue = function (_Queue) {
  _inherits(PriorityQueue, _Queue);

  function PriorityQueue() {
    _classCallCheck(this, PriorityQueue);

    return _possibleConstructorReturn(this, (PriorityQueue.__proto__ || Object.getPrototypeOf(PriorityQueue)).call(this));
  }

  _createClass(PriorityQueue, [{
    key: 'enqueue',
    value: function enqueue(element, priority) {
      var queueElement = this.queueElement(element, priority);
      if (this.isEmpty()) {
        this.items.push(queueElement);
      } else {
        var added = false;
        for (var i = 0; i < this.items.length; i++) {
          if (queueElement.priority < this.items[i].priority) {
            this.items.splice(i, 0, queueElement);
            added = true;
            break;
          }
        }
        if (!added) {
          this.items.push(queueElement);
        }
      }
    }
  }, {
    key: 'queueElement',
    value: function queueElement() {
      var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var priority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      return {
        element: element,
        priority: priority
      };
    }
  }, {
    key: 'print',
    value: function print() {
      var printv = [];
      for (var i = 0; i < this.items.length; i++) {
        printv.push(this.items[i].element);
      }
    }
  }]);

  return PriorityQueue;
}(_queue2.default);

exports.default = PriorityQueue;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL21vZHVsZS9ydW50aW1lL3ByaW9yaXR5cXVldWUuanMiXSwibmFtZXMiOlsiUHJpb3JpdHlRdWV1ZSIsImVsZW1lbnQiLCJwcmlvcml0eSIsInF1ZXVlRWxlbWVudCIsImlzRW1wdHkiLCJpdGVtcyIsInB1c2giLCJhZGRlZCIsImkiLCJsZW5ndGgiLCJzcGxpY2UiLCJwcmludHYiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7OztJQUVxQkEsYTs7O0FBQ25CLDJCQUFhO0FBQUE7O0FBQUE7QUFFWjs7Ozs0QkFDT0MsTyxFQUFRQyxRLEVBQVM7QUFDdkIsVUFBTUMsZUFBZSxLQUFLQSxZQUFMLENBQWtCRixPQUFsQixFQUEwQkMsUUFBMUIsQ0FBckI7QUFDQSxVQUFHLEtBQUtFLE9BQUwsRUFBSCxFQUFrQjtBQUNoQixhQUFLQyxLQUFMLENBQVdDLElBQVgsQ0FBZ0JILFlBQWhCO0FBQ0QsT0FGRCxNQUVLO0FBQ0gsWUFBSUksUUFBUSxLQUFaO0FBQ0EsYUFBSSxJQUFJQyxJQUFJLENBQVosRUFBZUEsSUFBRSxLQUFLSCxLQUFMLENBQVdJLE1BQTVCLEVBQW9DRCxHQUFwQyxFQUF3QztBQUN0QyxjQUFHTCxhQUFhRCxRQUFiLEdBQXdCLEtBQUtHLEtBQUwsQ0FBV0csQ0FBWCxFQUFjTixRQUF6QyxFQUFrRDtBQUNoRCxpQkFBS0csS0FBTCxDQUFXSyxNQUFYLENBQWtCRixDQUFsQixFQUFvQixDQUFwQixFQUFzQkwsWUFBdEI7QUFDQUksb0JBQVEsSUFBUjtBQUNBO0FBQ0Q7QUFDRjtBQUNELFlBQUcsQ0FBQ0EsS0FBSixFQUFVO0FBQ1IsZUFBS0YsS0FBTCxDQUFXQyxJQUFYLENBQWdCSCxZQUFoQjtBQUNEO0FBQ0Y7QUFDRjs7O21DQUNzQztBQUFBLFVBQTFCRixPQUEwQix1RUFBaEIsRUFBZ0I7QUFBQSxVQUFiQyxRQUFhLHVFQUFGLENBQUU7O0FBQ3JDLGFBQU87QUFDTEQsaUJBQVFBLE9BREg7QUFFTEMsa0JBQVNBO0FBRkosT0FBUDtBQUlEOzs7NEJBQ007QUFDTCxVQUFJUyxTQUFTLEVBQWI7QUFDQSxXQUFJLElBQUlILElBQUksQ0FBWixFQUFjQSxJQUFFLEtBQUtILEtBQUwsQ0FBV0ksTUFBM0IsRUFBa0NELEdBQWxDLEVBQXNDO0FBQ3BDRyxlQUFPTCxJQUFQLENBQVksS0FBS0QsS0FBTCxDQUFXRyxDQUFYLEVBQWNQLE9BQTFCO0FBQ0Q7QUFDRjs7Ozs7O2tCQWpDa0JELGEiLCJmaWxlIjoicHJpb3JpdHlxdWV1ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBRdWV1ZSBmcm9tICcuL3F1ZXVlJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQcmlvcml0eVF1ZXVlIGV4dGVuZHMgUXVldWUge1xuICBjb25zdHJ1Y3Rvcigpe1xuICAgIHN1cGVyKClcbiAgfVxuICBlbnF1ZXVlKGVsZW1lbnQscHJpb3JpdHkpe1xuICAgIGNvbnN0IHF1ZXVlRWxlbWVudCA9IHRoaXMucXVldWVFbGVtZW50KGVsZW1lbnQscHJpb3JpdHkpXG4gICAgaWYodGhpcy5pc0VtcHR5KCkpe1xuICAgICAgdGhpcy5pdGVtcy5wdXNoKHF1ZXVlRWxlbWVudClcbiAgICB9ZWxzZXtcbiAgICAgIHZhciBhZGRlZCA9IGZhbHNlXG4gICAgICBmb3IobGV0IGkgPSAwOyBpPHRoaXMuaXRlbXMubGVuZ3RoOyBpKyspe1xuICAgICAgICBpZihxdWV1ZUVsZW1lbnQucHJpb3JpdHkgPCB0aGlzLml0ZW1zW2ldLnByaW9yaXR5KXtcbiAgICAgICAgICB0aGlzLml0ZW1zLnNwbGljZShpLDAscXVldWVFbGVtZW50KVxuICAgICAgICAgIGFkZGVkID0gdHJ1ZVxuICAgICAgICAgIGJyZWFrXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmKCFhZGRlZCl7XG4gICAgICAgIHRoaXMuaXRlbXMucHVzaChxdWV1ZUVsZW1lbnQpXG4gICAgICB9XG4gICAgfVxuICB9XG4gIHF1ZXVlRWxlbWVudChlbGVtZW50ID0gJycscHJpb3JpdHkgPSAwKXtcbiAgICByZXR1cm4ge1xuICAgICAgZWxlbWVudDplbGVtZW50LFxuICAgICAgcHJpb3JpdHk6cHJpb3JpdHlcbiAgICB9XG4gIH1cbiAgcHJpbnQoKXtcbiAgICB2YXIgcHJpbnR2ID0gW11cbiAgICBmb3IobGV0IGkgPSAwO2k8dGhpcy5pdGVtcy5sZW5ndGg7aSsrKXtcbiAgICAgIHByaW50di5wdXNoKHRoaXMuaXRlbXNbaV0uZWxlbWVudClcbiAgICB9XG4gIH1cbn0iXX0=