'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _queue = require('./queue');

var _queue2 = _interopRequireDefault(_queue);

var _event = require('../constants/event');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Event = function () {
  function Event(_ref) {
    var _ref$times = _ref.times,
        times = _ref$times === undefined ? _event.EventProcessTimes.INFINITY : _ref$times,
        cb = _ref.cb;

    _classCallCheck(this, Event);

    this.times = times;
    this.callback = cb;
    this.deleted = false;
  }

  _createClass(Event, [{
    key: 'deleted',
    set: function set(deleted) {}
  }]);

  return Event;
}();

var EventEmitter = function () {
  function EventEmitter() {
    _classCallCheck(this, EventEmitter);

    this.eventQueues = {};
  }

  _createClass(EventEmitter, [{
    key: 'find',
    value: function find(name) {
      if (this.eventQueues.keys().indexOf(name) != -1) {
        return this.eventQueues[name];
      }
      var cbQueue = new _queue2.default();
      return this.eventQueues[name] = cbQueue;
    }
  }, {
    key: 'on',
    value: function on(name, cb) {
      if (!cb || typeof cb !== 'function') throw new Error('callback is not a function');
      var event = this.find(name);
      event.enqueue(new Event(_event.EventProcessTimes.INFINITY, cb));
    }
  }, {
    key: 'once',
    value: function once(name, cb) {
      if (!cb || typeof cb !== 'function') throw new Error('callback is not a function');
      var event = this.find(name);
      event.enqueue(new Event(_event.EventProcessTimes.ONCE, cb));
    }
  }, {
    key: 'emit',
    value: function emit(name) {
      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var events = this.find(name);
      events.each(function (event, index) {
        event.callback(value);
        if (event.times == _event.EventProcessTimes.ONCE) {
          event.deleted = true;
        }
      });
      events.clearToFlag('deleted');
    }
  }, {
    key: 'remove',
    value: function remove(name, callback) {
      var event = this.find(name);
    }
  }]);

  return EventEmitter;
}();

exports.default = EventEmitter;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL21vZHVsZS9ydW50aW1lL2V2ZW50LmpzIl0sIm5hbWVzIjpbIkV2ZW50IiwidGltZXMiLCJJTkZJTklUWSIsImNiIiwiY2FsbGJhY2siLCJkZWxldGVkIiwiRXZlbnRFbWl0dGVyIiwiZXZlbnRRdWV1ZXMiLCJuYW1lIiwia2V5cyIsImluZGV4T2YiLCJjYlF1ZXVlIiwiRXJyb3IiLCJldmVudCIsImZpbmQiLCJlbnF1ZXVlIiwiT05DRSIsInZhbHVlIiwiZXZlbnRzIiwiZWFjaCIsImluZGV4IiwiY2xlYXJUb0ZsYWciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O0lBQ01BLEs7QUFDSix1QkFBa0Q7QUFBQSwwQkFBckNDLEtBQXFDO0FBQUEsUUFBckNBLEtBQXFDLDhCQUEvQix5QkFBa0JDLFFBQWE7QUFBQSxRQUFKQyxFQUFJLFFBQUpBLEVBQUk7O0FBQUE7O0FBQzlDLFNBQUtGLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtHLFFBQUwsR0FBZ0JELEVBQWhCO0FBQ0EsU0FBS0UsT0FBTCxHQUFlLEtBQWY7QUFDSDs7OztzQkFDV0EsTyxFQUFTLENBRXBCOzs7Ozs7SUFFa0JDLFk7QUFDbkIsMEJBQWE7QUFBQTs7QUFDVCxTQUFLQyxXQUFMLEdBQW1CLEVBQW5CO0FBQ0g7Ozs7eUJBQ0lDLEksRUFBSztBQUNQLFVBQUcsS0FBS0QsV0FBTCxDQUFpQkUsSUFBakIsR0FBd0JDLE9BQXhCLENBQWdDRixJQUFoQyxLQUF1QyxDQUFDLENBQTNDLEVBQTZDO0FBQzFDLGVBQU8sS0FBS0QsV0FBTCxDQUFpQkMsSUFBakIsQ0FBUDtBQUNGO0FBQ0QsVUFBSUcsVUFBVSxxQkFBZDtBQUNBLGFBQU8sS0FBS0osV0FBTCxDQUFpQkMsSUFBakIsSUFBeUJHLE9BQWhDO0FBQ0Y7Ozt1QkFDRUgsSSxFQUFLTCxFLEVBQUc7QUFDVCxVQUFHLENBQUNBLEVBQUQsSUFBTyxPQUFPQSxFQUFQLEtBQWMsVUFBeEIsRUFBb0MsTUFBTSxJQUFJUyxLQUFKLENBQVUsNEJBQVYsQ0FBTjtBQUNwQyxVQUFJQyxRQUFRLEtBQUtDLElBQUwsQ0FBVU4sSUFBVixDQUFaO0FBQ0FLLFlBQU1FLE9BQU4sQ0FBYyxJQUFJZixLQUFKLENBQVUseUJBQWtCRSxRQUE1QixFQUFxQ0MsRUFBckMsQ0FBZDtBQUNEOzs7eUJBQ0lLLEksRUFBS0wsRSxFQUFHO0FBQ1gsVUFBRyxDQUFDQSxFQUFELElBQU8sT0FBT0EsRUFBUCxLQUFjLFVBQXhCLEVBQW9DLE1BQU0sSUFBSVMsS0FBSixDQUFVLDRCQUFWLENBQU47QUFDcEMsVUFBSUMsUUFBUSxLQUFLQyxJQUFMLENBQVVOLElBQVYsQ0FBWjtBQUNBSyxZQUFNRSxPQUFOLENBQWMsSUFBSWYsS0FBSixDQUFVLHlCQUFrQmdCLElBQTVCLEVBQWlDYixFQUFqQyxDQUFkO0FBQ0Q7Ozt5QkFDSUssSSxFQUFjO0FBQUEsVUFBVFMsS0FBUyx1RUFBSCxFQUFHOztBQUNqQixVQUFJQyxTQUFTLEtBQUtKLElBQUwsQ0FBVU4sSUFBVixDQUFiO0FBQ0FVLGFBQU9DLElBQVAsQ0FBWSxVQUFDTixLQUFELEVBQU9PLEtBQVAsRUFBZTtBQUN4QlAsY0FBTVQsUUFBTixDQUFlYSxLQUFmO0FBQ0EsWUFBR0osTUFBTVosS0FBTixJQUFlLHlCQUFrQmUsSUFBcEMsRUFBeUM7QUFDdkNILGdCQUFNUixPQUFOLEdBQWdCLElBQWhCO0FBQ0Q7QUFDSCxPQUxEO0FBTUFhLGFBQU9HLFdBQVAsQ0FBbUIsU0FBbkI7QUFDRDs7OzJCQUNNYixJLEVBQUtKLFEsRUFBUztBQUNuQixVQUFJUyxRQUFRLEtBQUtDLElBQUwsQ0FBVU4sSUFBVixDQUFaO0FBQ0Q7Ozs7OztrQkFqQ2tCRixZIiwiZmlsZSI6ImV2ZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgUXVldWUgZnJvbSAnLi9xdWV1ZSdcbmltcG9ydCB7IEV2ZW50UHJvY2Vzc1RpbWVzIH0gZnJvbSAnLi4vY29uc3RhbnRzL2V2ZW50J1xuY2xhc3MgRXZlbnQge1xuICBjb25zdHJ1Y3Rvcih7dGltZXM9RXZlbnRQcm9jZXNzVGltZXMuSU5GSU5JVFksY2J9KXtcbiAgICAgIHRoaXMudGltZXMgPSB0aW1lc1xuICAgICAgdGhpcy5jYWxsYmFjayA9IGNiXG4gICAgICB0aGlzLmRlbGV0ZWQgPSBmYWxzZVxuICB9XG4gIHNldCBkZWxldGVkKGRlbGV0ZWQpIHtcbiAgICAgXG4gIH1cbn1cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEV2ZW50RW1pdHRlciB7XG4gIGNvbnN0cnVjdG9yKCl7XG4gICAgICB0aGlzLmV2ZW50UXVldWVzID0ge31cbiAgfVxuICBmaW5kKG5hbWUpe1xuICAgICBpZih0aGlzLmV2ZW50UXVldWVzLmtleXMoKS5pbmRleE9mKG5hbWUpIT0tMSl7XG4gICAgICAgIHJldHVybiB0aGlzLmV2ZW50UXVldWVzW25hbWVdXG4gICAgIH1cbiAgICAgbGV0IGNiUXVldWUgPSBuZXcgUXVldWUoKVxuICAgICByZXR1cm4gdGhpcy5ldmVudFF1ZXVlc1tuYW1lXSA9IGNiUXVldWVcbiAgfVxuICBvbihuYW1lLGNiKXtcbiAgICBpZighY2IgfHwgdHlwZW9mIGNiICE9PSAnZnVuY3Rpb24nKSB0aHJvdyBuZXcgRXJyb3IoJ2NhbGxiYWNrIGlzIG5vdCBhIGZ1bmN0aW9uJylcbiAgICBsZXQgZXZlbnQgPSB0aGlzLmZpbmQobmFtZSlcbiAgICBldmVudC5lbnF1ZXVlKG5ldyBFdmVudChFdmVudFByb2Nlc3NUaW1lcy5JTkZJTklUWSxjYikpXG4gIH1cbiAgb25jZShuYW1lLGNiKXtcbiAgICBpZighY2IgfHwgdHlwZW9mIGNiICE9PSAnZnVuY3Rpb24nKSB0aHJvdyBuZXcgRXJyb3IoJ2NhbGxiYWNrIGlzIG5vdCBhIGZ1bmN0aW9uJylcbiAgICBsZXQgZXZlbnQgPSB0aGlzLmZpbmQobmFtZSlcbiAgICBldmVudC5lbnF1ZXVlKG5ldyBFdmVudChFdmVudFByb2Nlc3NUaW1lcy5PTkNFLGNiKSlcbiAgfVxuICBlbWl0KG5hbWUsdmFsdWU9e30pe1xuICAgIGxldCBldmVudHMgPSB0aGlzLmZpbmQobmFtZSlcbiAgICBldmVudHMuZWFjaCgoZXZlbnQsaW5kZXgpPT57XG4gICAgICAgZXZlbnQuY2FsbGJhY2sodmFsdWUpXG4gICAgICAgaWYoZXZlbnQudGltZXMgPT0gRXZlbnRQcm9jZXNzVGltZXMuT05DRSl7XG4gICAgICAgICBldmVudC5kZWxldGVkID0gdHJ1ZVxuICAgICAgIH1cbiAgICB9KVxuICAgIGV2ZW50cy5jbGVhclRvRmxhZygnZGVsZXRlZCcpXG4gIH1cbiAgcmVtb3ZlKG5hbWUsY2FsbGJhY2spe1xuICAgIGxldCBldmVudCA9IHRoaXMuZmluZChuYW1lKVxuICB9XG59Il19