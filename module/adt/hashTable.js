import chainTable from './chainTable'
import {nextPrime} from '../runtime/nextPrime'
export default class hashTree{
  constructor(size){
    this.tableSize = nextPrime(size)
    console.log(this.tableSize)
    this.table = new Array(this.tableSize).fill(0) 
  }
  hash(x){
    let key = x+'',i = 0,len = key.length,hashVal = 0
    while(len!==i){
      hashVal = (hashVal<<5) + key[i++]
    }
    return hashVal % this.tableSize
  }
  insert(x){
    let hash = this.hash(x)
    let ctable = this.table[hash]
    if(!ctable){
      ctable = new chainTable()
      this.table[hash] = ctable
    }
    
    ctable.insert(x)
  }
  find(x){
    let ctable = this.table[this.hash(x)]
    return ctable.find(x)
  }
}