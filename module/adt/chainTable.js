
const node = {
  element:'',
  next:''
}
export default class chainTable{
  constructor(){
    this.header = {...node}
  }
  insert(x){
    let p = this.header
    while(p.element!==''&&p.element!==x){
       if(p.next===''){
         p.next = {...node}
       }
       p = p.next
    }
    p.element = x
  }
  find(x){
    let p = this.header
    while(p.element!==x&&p.next!==''){
      p = p.next
    }
    return p
  }
  findPrevious(x){
    let p = this.header
    while(p.next.element!==x&&p.next!==''){
      p = p.next
    }
    return p
  }
  delete(x){
    let p = this.findPrevious(x)
    if(p){
       let cur = p.next
       p.next = cur.next
    }
  }
}