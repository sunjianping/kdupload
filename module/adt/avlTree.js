import binaryTree from './binaryTree'

const tree = {
  element:'',
  left:'',
  right:'',
  height:0
}
let TreeNode = {...tree}

export default class avlTree extends binaryTree{
  constructor(){
    super()
  }
  getTree(){
    return TreeNode
  }
  insert(x){
    TreeNode = this.insertNode(x,TreeNode)
  }
  insertNode(x,T){
    if(T.element===''){
      T.element = x
      T.height = 0
      T.right = {...tree}
      T.left = {...tree}
    }else if(x<T.element){
      T.left = this.insertNode(x,T.left)
      if(T.left.height - T.right.height == 2){
        if(x<T.left.element){
          T = this.singleRotateWithLeft(T)
        }else{
          T = this.doubleRotateWithLeft(T)
        }
      }
    }else if(x>T.element){
      T.right = this.insertNode(x,T.right)
      if(T.right.height - T.left.height == 2){
        if(x>T.right.element){
          T = this.singleRotateWithRight(T)
        }else{
          T = this.doubleRotateWithRight(T)
        }
      }
    }
    T.height =Math.max(T.left.height,T.right.height) +1
    return T
  }
  singleRotateWithLeft(T){
    let T1 = T.left
    T.left = T1.right
    T1.right = T
    T.height = Math.max(T.left.height,T.right.height) + 1
    T1.height = Math.max(T1.left.height,T1.right.height) + 1
    return T1
  }
  singleRotateWithRight(T){
    let T1 = T.right
    T.right = T1.left
    T1.left = T
    T.height = Math.max(T.left.height,T.right.height) + 1
    T1.height = Math.max(T1.left.height,T1.right.height) + 1
    return T1
  }
  doubleRotateWithLeft(T){
    T.left = this.singleRotateWithRight(T.left)
    return this.singleRotateWithLeft(T)
  }
  doubleRotateWithRight(T){
    T.right = this.singleRotateWithLeft(T.right)
    return this.singleRotateWithRight(T)
  }
}