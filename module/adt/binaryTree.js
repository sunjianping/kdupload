const tree = {
   element:'',
   left:'',
   right:'',
   height:0
}
let TreeNode = {...tree}
export default class binaryTree {
  constructor(){
    
  }
  getTree(){
    return TreeNode
  }
  insert(x,T=TreeNode){
    if(T.element===''){
      T.element = x
      T.right = {...tree}
      T.left = {...tree}
    }else if(x<T.element){
      T.left = this.insert(x,T.left)
    }else if(x>T.element){
      T.right = this.insert(x,T.right)
    }
    return T
  }
  findMin(T=TreeNode){
    if(T.left&&T.left.element!==''){
      return this.findMin(T.left)
    }else if(T.element!==''){
      return T
    }else{
      return false
    }
  }
  findMax(T=TreeNode){
    if(T.right&&T.right.element!==''){
      return this.findMax(T.right)
    }else if(T.element!==''){
      return T
    }else{
      return false
    }
  }
  Delete(x,T=TreeNode){
    if(T.element === ''){
      return false
    }else if(x<T.element){
      this.Delete(x,T.left)
    }else if(x>T.element){
      this.Delete(x,T.right)
    }else if(T.right&&T.left&&T.left.element!==''&&T.right.element!==''){
      let minT = this.findMin(T.right)
      T.element = minT.element
      T.right = this.Delete(T.element,T.right)
    }else{
      let minT = T
      if(T.left.element === ''){
        T = T.right
      }else if(T.right.element === ''){
        T = T.left
      }
    }
    return T
  }
  print(T=TreeNode){
    if(T.element!==''){
      if(T.left.element!==''){
        this.print(T.left)
      }
      console.log(T.element)
      if(T.right.element!==''){
        this.print(T.right)
      }
      
    }
  }
  makeEmpty(){

  }
}