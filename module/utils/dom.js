


export default function trigger(event){
  //IE
      if(document.all) {
          this[event]()
      }
  // 其它浏览器
      else {
        var e = document.createEvent('MouseEvents')
        e.initEvent(event, true, true)　　　　　　　　　　　　　　//这里的click可以换成你想触发的行为
        this.dispatchEvent(e)　　　//这里的clickME可以换成你想触发行为的DOM结点
      }   
  }
  