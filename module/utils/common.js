

export function isArray(obj){
  return Object.prototype.toString.call(obj).toLowerCase() === '[object array]'
}

export function noop(){
  
}