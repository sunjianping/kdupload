import fileMd5 from './runtime/md5'
import EventEmitter from './runtime/event'
import Queue from './runtime/queue'
import trigger from './utils/dom'
import {isArray} from './utils/common'
import { type } from 'os'
class Option {
    constructor({server='',btn='',accept='',dnd=undefined,multiple=false}){
        this.dnd = dnd
        this.server = server
        this.btn = btn
        this.accept = accept
        this.multiple = multiple
    }
}
class FileInstane {
    constructor(id,{accept='',multiple=''}){
        this.id = id
        this.accept  = accept
        this.multiple = multiple

        this.append(this.getPick(id,accept,multiple))
    }
    getPick(id,accept,multiple){
        let pick = document.createElement('INPUT')
        pick.setAttribute('style','opacity:0')
        pick.setAttribute('type','file')
        pick.setAttribute('id',id)
        pick.setAttribute('accept',accept)
        pick.setAttribute('multiple',multiple)
        return pick
    }
    instance(){
        return document.querySelector('#'+this.id)
    }
    append(pick){
        document.body.appendChild(pick)
    }
    delete(){
        document.body.remove(this.instance())
    }
}
export default class KdPanUploader extends EventEmitter{

    constructor (options = {}){
        super()
       this.options = new Option(options)
       this.files = new Queue()
       this.instance = options.btn ?this.fileInstance():null
       this.events()
    }
    kd_ID(){
        if(!this.options.id){
            return 'KDUPLODER_ID_'+Date.now()
        }
        return this.options.id
    }
    bindInstanceClick(pickBtns){
        if(pickBtns.length>0){
            pickBtns.forEach((btn,index)=>{
                btn.addEventListener('click',(e)=>{
                    trigger.call(this.instance,'click')
                    e.stopPropagation()
                }) 
            })
        }else{
            pickBtns.addEventListener('click',(e)=>{
                trigger.call(this.instance,'click')
                e.stopPropagation()
            }) 
        }
         
    }
    events(){
        if(this.options.btn){
            let pickBtns = this.options.btn
            if(typeof pickBtns == 'string'){  
                pickBtns = document.querySelectorAll(pickBtns)
            }
            this.bindInstanceClick(pickBtns)
        }
        if(this.instance){
            this.instance.addEventListener('change',(e)=>{
                this.emit('fileQueued',e.target.files)
            })
        }
    }
    fileInstance(){
        return new FileInstane(this.kd_ID(),this.options).instance()
    }
    option(){

    }
    upload(file){

    }
    md5(file){
        return new fileMd5(file,this.options)
    }
}