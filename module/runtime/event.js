
import Queue from './queue'
import { EventProcessTimes } from '../constants/event'
import {noop} from '../utils/common'
class Event {
  constructor(times=EventProcessTimes.INFINITY,cb=noop){
      this.times = times
      this.callback = cb 
      this.deleted = false
  }
  set deleted(deleted) {
     
  }
}
export default class EventEmitter {
  constructor(){
      this.eventQueues = {}
  }
  find(name){
     if(Object.keys(this.eventQueues).indexOf(name)!=-1){
        return this.eventQueues[name]
     }
     let cbQueue = new Queue()
     return this.eventQueues[name] = cbQueue
  }
  on(name,cb){
    if(!cb || typeof cb !== 'function') throw new Error('callback is not a function')
    let event = this.find(name)
    event.enqueue(new Event(EventProcessTimes.INFINITY,cb))
    return this
  }
  once(name,cb){
    if(!cb || typeof cb !== 'function') throw new Error('callback is not a function')
    let event = this.find(name)
    event.enqueue(new Event(EventProcessTimes.ONCE,cb))
    return this
  }
  emit(name,value={}){
    let events = this.find(name)
    events.each((event,index)=>{
       event.callback(value)
       if(event.times == EventProcessTimes.ONCE){
         event.deleted = true
       }
    })
    events.clearToFlag('deleted',true)
  }
  remove(name,cb){
    let events = this.find(name)
    if(!cb){
        events.clear()
    }else{
        events.clearToFlag('callback',cb)
    }
  }
}