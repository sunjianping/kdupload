


export function nextPrime(m){
  let prime = m,found = false
  while(!found){
    let n = Math.sqrt(prime),i = 2
    for(i=2;i<=n;i++){
      if(prime%i===0){
        break
      }
    }
    if(i>n){
      break
    }
    prime++
  }
  return prime
}