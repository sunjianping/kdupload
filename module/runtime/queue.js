export default class Queue {
  constructor(){
    this.items = []
  }
  enqueue(element){
    this.items.push(element)
  }
  dequeue(){
    return this.items.shift()
  }
  front(){
    return this.items[0]
  }
  isEmpty(){
    return this.items.length == 0
  }
  size(){
    return this.items.length
  }
  each(cb){
    this.items.forEach(cb)    
  }
  clear(){
    this.items = []
  }
  clearToFlag(flag,value){
    let len = this.items.length , i = 0
    for(;i<len;i++){
      let item = this.items[i]
      if(item[flag] === value){
        this.items.splice(i,1)
        i--
        len--
      }
    }
  }
}