import SparkMD5 from 'spark-md5'
import { CHUNK } from '../constants/chunk'
import EventEmitter from './event'

export default class fileMd5 extends EventEmitter{
    constructor(file,options){
        super()
        this.file = file
        this.loadedTotal = 0
        this.chunkSize = options.chunkSize || CHUNK.SIZE                             // Read in chunks of 2MB
        this.chunks = Math.ceil(file.size / this.chunkSize)
        this.currentChunk = 0
        this.spark = new SparkMD5.ArrayBuffer()
        this.fileReader = new FileReader()
        this.fileReader.onload = this.onload.bind(this)
        this.fileReader.onerror = this.onerror.bind(this)
        this.fileReader.onprogress = this.onProcess.bind(this)
        this.loadNext()
    }

    blobSlice(file,...param){
        let slice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice
        return slice.apply(file,param)
    }
    loadNext(){
        var start = this.currentChunk * this.chunkSize,
                end = ((start + this.chunkSize) >= this.file.size) ? this.file.size : start + this.chunkSize
        console.log('loading')
        this.fileReader.readAsArrayBuffer(this.blobSlice(this.file, start, end))
    }
    onerror(){
        console.warn('oops, something went wrong.')
    }
    onload(e){
        console.log('read chunk nr', this.currentChunk + 1, 'of', this.chunks)
        this.spark.append(e.target.result)                   // Append array buffer
        this.currentChunk++

        if (this.currentChunk < this.chunks) {
            this.loadNext()
        } else {
            this.emit('finished',this.spark.end())
        }
    }
    onProcess({loaded,total}){
        this.loadedTotal += loaded
        let percentage = this.loadedTotal/this.file.size
        this.emit('process',Math.min(percentage,1))
    }
}
