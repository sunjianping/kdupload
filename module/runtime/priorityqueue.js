import Queue from './queue'

export default class PriorityQueue extends Queue {
  constructor(){
    super()
  }
  enqueue(element,priority){
    const queueElement = this.queueElement(element,priority)
    if(this.isEmpty()){
      this.items.push(queueElement)
    }else{
      var added = false
      for(let i = 0; i<this.items.length; i++){
        if(queueElement.priority < this.items[i].priority){
          this.items.splice(i,0,queueElement)
          added = true
          break
        }
      }
      if(!added){
        this.items.push(queueElement)
      }
    }
  }
  queueElement(element = '',priority = 0){
    return {
      element:element,
      priority:priority
    }
  }
  print(){
    var printv = []
    for(let i = 0;i<this.items.length;i++){
      printv.push(this.items[i].element)
    }
  }
}