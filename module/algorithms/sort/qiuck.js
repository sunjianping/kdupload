
const qiuck = (arr,i,n)=>{
    let index = 0
    if(arr.length>1){
        index = partition(arr,i,n)
        if(i<index-1){
          qiuck(arr,i,index-1)
        }
        if(n>index+1){
          qiuck(arr,index,n)
        }
    }
}

const partition = (arr,left,right)=>{
    let pivot = arr[Math.floor((left+right)/2)],
        i = left , j = right
    while(i<=j){
      while(arr[i]<pivot){
        i++
      }
      while(arr[j]>pivot){
        j--
      }
      if(i<=j){
         let mid = arr[i]
         arr[i] = arr[j]
         arr[j] = mid
         i++
         j--
      }
    }
    return i
}






export function qiuckSort(arr,i,n){
   return qiuck(arr,i,n)
}
