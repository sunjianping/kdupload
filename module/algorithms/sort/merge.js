const mergeSortRec = (arr)=>{
  let len=arr.length
  if(len === 1){
    return arr
  }
  let middle = Math.floor(len/2),
  left = arr.slice(0,middle),
  right = arr.slice(middle,len)
  return merge(mergeSortRec(left),mergeSortRec(right))
}
const merge = (left,right)=>{
  let li = 0,ri = 0,llen = left.length,rlen = right.length,ret = []
  while(li<llen&&ri<rlen){
     if(left[li]<=right[ri]){
       ret.push(left[li++])
     }else{
       ret.push(right[ri++])
     }
  }

  while(li<llen){
    ret.push(left[li++])
  }
  while(ri<rlen){
    ret.push(right[ri++])
  }
  return ret
}
export function mergeSort(arr){
  return mergeSortRec(arr)
}