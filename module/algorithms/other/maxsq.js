

export function maxSubsequenceSum(arr,n){
   let csum = 0,maxsum = 0,len = arr.length-1
   for(let i= 0;i<=n&&i<=len;i++){
     csum +=arr[i]
     if(csum > maxsum){
        maxsum = csum
     }else{
       csum = 0
     }
   }
   return maxsum
}