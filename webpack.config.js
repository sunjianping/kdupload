

let path = require('path')

module.exports = {
    entry:path.resolve(__dirname,'./index.js'),
    output:{
      path:path.resolve(__dirname,'./dist'),
      filename:'index.js',
      library: 'kdUploader',
      libraryTarget:'umd'
    },
    mode:'development',
    module:{
      rules:[
        {
          test:/\.js(x)?$/,
          exclude: /node_modules/,
          use:['babel-loader']
        }
      ]
    }
}